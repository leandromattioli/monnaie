package monnaie.gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class ImageCanvas extends JPanel {
    
	private static final long serialVersionUID = 230L;
	private BufferedImage img = null;
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(img != null)
            g.drawImage(img, 0, 0, null);
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }
}

