package monnaie.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import monnaie.img.Camera;
import monnaie.img.CircleImage;
import monnaie.lvq.LVQNetwork;
import monnaie.lvq.LVQTrainer;
import monnaie.lvq.PatternTable;
import monnaie.utils.CoinEventHandler;
import monnaie.utils.Constants;

import javax.swing.JSeparator;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JSpinner;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.SpinnerNumberModel;

public class MainWindow extends JFrame {
	
    static { System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }
	
    //App Related
    private CoinEventHandler handler;
    private float total;
    
	//Network related
	private LVQNetwork network;
	private LVQTrainer trainer;
	private PatternTable trainingSet;
	private PatternTable testSet;
	private ArrayList<Byte> features;
	
	//Camera related
	private Camera cam;
	private ScheduledExecutorService scheduler;
	private BufferedImage preview;
	private CircleImage circle;
	private final Size previewSize = new Size(Constants.PREVIEW_DIAMETER, Constants.PREVIEW_DIAMETER);
	
	//GUI Related
	private ImageCanvas canvas1;
	private ImageCanvas previewCanvas;
	private JPanel contentPane;
	private static final long serialVersionUID = 1L;
	private JPanel controlPanel;
	private JButton btnNewButton;
	private JRadioButton radio1Cent;
	private JRadioButton radio5Cents;
	private JRadioButton radio10Cents;
	private JRadioButton radio25Cents;
	private JRadioButton radio50Cents;
	private JRadioButton radio1Real;
	private final ButtonGroup groupCluster = new ButtonGroup();
	private JLabel labelCluster;
	private JPanel imagesPanel;
	private ImageCanvas canvas3;
	private ImageCanvas canvas2;
	private ImageCanvas canvas4;
	private JLabel lblErros;
	private JPanel panelTraining;
	private JLabel lblNewLabel;
	private JLabel label;
	private JSpinner spinAlpha1;
	private JSpinner spinAlpha2;
	private JLabel lblEpocas;
	private JSpinner spinEpochs;
	private JButton btnTreinar;
	private JLabel lblErrorCount;
	private JPanel panelTeste;
	private JPanel panelConjTreinamento;
	private JPanel panelTotal;
	private JLabel lblTotal;
	
	private void configCore() throws IOException {
		//App related
		handler = new CoinEventHandler();
		//Network related
		network = new LVQNetwork((short) 5, Constants.CLASSES);
		network.initWeights();
		network.toBinFile("out/initialWeights.bin");
		trainer = new LVQTrainer();
		trainer.setNetwork(network);
		trainingSet = trainer.generateFeaturesTrainingSet();
		testSet = trainer.generateFeaturesTestSet();
		updateTrainerParams();
		train();
		
		//Camera related
		cam = new Camera();
		scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new UpdateCanvas(), 100, 100, TimeUnit.MILLISECONDS);
	}
	
	private void updateTrainerParams() {
		trainer.setInitialAlpha((float) spinAlpha1.getValue());
		trainer.setFinalAlpha((float) spinAlpha2.getValue());
		trainer.setEpochs((int) spinEpochs.getValue());
	}
    
	
	class UpdateCanvas implements Runnable {
        @Override
        public void run() {
            try {
                if(!cam.isBusy()) {
                    cam.grabFrame();
                    cam.preProcess();
                    canvas1.setImg(cam.getImage());
                    canvas2.setImg(cam.getGrayImage());
                    canvas3.setImg(cam.getContoursImage());
                    canvas4.setImg(cam.getThresholdedImage());
                    canvas1.repaint();
                    canvas2.repaint();
                    canvas3.repaint();
                    canvas4.repaint();
                    if(cam.detectCircles()) {
                    	preview = cam.getCircleBGR(previewSize);
                        previewCanvas.setImg(preview);
                        previewCanvas.repaint();
                        circle = new CircleImage(cam.getCircleYCrCbMat());
                        features = circle.getFeatures();
                        Short cluster = network.classify(features);
                        labelCluster.setText(String.format(" R$ %.2f", Constants.clusterMap[cluster]));
                        handler.addCoinEvent(cluster);
                        cam.showValue(cluster);
                        canvas1.setImg(cam.getImage());
                        canvas1.repaint();
                    }
                    else {
                    	handler.addNoneEvent();
                    }
                    total = handler.getMoney();
                    lblTotal.setText(String.format("R$ %.2f", total));
                    if(handler.isLocked())
                    	lblTotal.setForeground(Color.RED);
                    else
                    	lblTotal.setForeground(Color.BLUE);
                }
                
            } 
            catch (IOException ex) {
                System.out.println("Erro de E/S!");
                System.exit(-1);
            }
        }
    }
	
	
	//GUI Utilities
	
	public short getSelectedCluster() {
		for (Enumeration<AbstractButton> buttons = groupCluster.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();
            if (button.isSelected()) {
                return Short.parseShort(button.getName());
            }
        }
		return -1;
	}
	
	//Training Related
	
	public void train() {
		try {
			//network.fromBinFile("out/initialWeights_featuresAvg.bin");
			//network.fromCSVFile("out/initialWeights_featuresAvg.csv");
			//network.toBinFile("out/networkWeights_featuresAvg.bin");
			network.fromBinFile("out/initialWeights.bin");
		} catch (IOException e) {
			System.err.println("Impossível ler pesos iniciais!");
			System.exit(ERROR);
		}
		updateTrainerParams();
		trainer.train(trainingSet);
		trainer.test(testSet);
		lblErrorCount.setText("" + trainer.getWrongCount());
		try {
			network.toBinFile("out/networkWeights.bin");
			network.toBinFile("out/networkWeights.csv");
		} catch (IOException e) {
			System.err.println("Impossível escrever pesos finais!");
			System.exit(ERROR);
		}
	}
	
	public void save() throws IOException {
		Mat circle = cam.getCircleBGRMat();
		Short cluster = getSelectedCluster();
		String fileName = Constants.IMG_TRAINING_PREFIX + "_";
		fileName += cluster.toString() + "_";
		fileName += Constants.dateFormat.format(new Date()) + ".bmp";
		Highgui.imwrite(fileName, circle);
	}
    

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public MainWindow() {
		setMinimumSize(new Dimension(600, 600));
		setMaximumSize(new Dimension(850, 700));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 856, 535);
		contentPane = new JPanel();
		contentPane.setSize(new Dimension(600, 600));
		contentPane.setMinimumSize(new Dimension(600, 600));
		contentPane.setMaximumSize(new Dimension(800, 600));
		contentPane.setPreferredSize(new Dimension(600, 600));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		imagesPanel = new JPanel();
		imagesPanel.setBorder(new TitledBorder(null, "Previs\u00E3o", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(imagesPanel);
		imagesPanel.setLayout(new GridLayout(2, 2, 0, 0));
		
		canvas1 = new ImageCanvas();
		imagesPanel.add(canvas1);
		canvas1.setPreferredSize(new Dimension(320, 240));
		canvas1.setMinimumSize(new Dimension(320, 240));
		canvas1.setMaximumSize(new Dimension(320, 240));
		canvas1.setSize(new Dimension(320, 240));
		canvas1.setSize(320, 240);
		FlowLayout fl_canvas1 = (FlowLayout) canvas1.getLayout();
		fl_canvas1.setAlignOnBaseline(true);
		canvas1.setVisible(true);
		
		canvas2 = new ImageCanvas();
		imagesPanel.add(canvas2);
		
		canvas3 = new ImageCanvas();
		canvas3.setPreferredSize(new Dimension(320, 240));
		canvas3.setMaximumSize(new Dimension(320, 240));
		canvas3.setMinimumSize(new Dimension(320, 240));
		imagesPanel.add(canvas3);
		
		canvas4 = new ImageCanvas();
		imagesPanel.add(canvas4);
		
		controlPanel = new JPanel();
		controlPanel.setBorder(null);
		controlPanel.setSize(new Dimension(300, 600));
		controlPanel.setPreferredSize(new Dimension(300, 600));
		controlPanel.setMaximumSize(new Dimension(300, 600));
		contentPane.add(controlPanel);
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
		
		panelTotal = new JPanel();
		controlPanel.add(panelTotal);
		
		lblTotal = new JLabel("R$ 0,00");
		lblTotal.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTotal.setForeground(Color.BLUE);
		panelTotal.add(lblTotal);
		
		panelConjTreinamento = new JPanel();
		panelConjTreinamento.setBorder(new TitledBorder(null, "Conj. Treinamento", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		controlPanel.add(panelConjTreinamento);
		GridBagLayout gbl_panelConjTreinamento = new GridBagLayout();
		gbl_panelConjTreinamento.columnWidths = new int[] {0};
		gbl_panelConjTreinamento.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelConjTreinamento.columnWeights = new double[]{1.0};
		gbl_panelConjTreinamento.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelConjTreinamento.setLayout(gbl_panelConjTreinamento);
		
		radio1Cent = new JRadioButton("1 centavo");
		GridBagConstraints gbc_radio1Cent = new GridBagConstraints();
		gbc_radio1Cent.fill = GridBagConstraints.HORIZONTAL;
		gbc_radio1Cent.insets = new Insets(0, 0, 5, 0);
		gbc_radio1Cent.gridx = 0;
		gbc_radio1Cent.gridy = 0;
		panelConjTreinamento.add(radio1Cent, gbc_radio1Cent);
		radio1Cent.setName("0");
		groupCluster.add(radio1Cent);
		
		radio5Cents = new JRadioButton("5 centavos");
		GridBagConstraints gbc_radio5Cents = new GridBagConstraints();
		gbc_radio5Cents.fill = GridBagConstraints.HORIZONTAL;
		gbc_radio5Cents.insets = new Insets(0, 0, 5, 0);
		gbc_radio5Cents.gridx = 0;
		gbc_radio5Cents.gridy = 1;
		panelConjTreinamento.add(radio5Cents, gbc_radio5Cents);
		radio5Cents.setName("1");
		groupCluster.add(radio5Cents);
		
		radio10Cents = new JRadioButton("10 centavos");
		GridBagConstraints gbc_radio10Cents = new GridBagConstraints();
		gbc_radio10Cents.fill = GridBagConstraints.HORIZONTAL;
		gbc_radio10Cents.insets = new Insets(0, 0, 5, 0);
		gbc_radio10Cents.gridx = 0;
		gbc_radio10Cents.gridy = 2;
		panelConjTreinamento.add(radio10Cents, gbc_radio10Cents);
		radio10Cents.setName("2");
		groupCluster.add(radio10Cents);
		radio10Cents.setSelected(true);
		
		radio25Cents = new JRadioButton("25 centavos");
		GridBagConstraints gbc_radio25Cents = new GridBagConstraints();
		gbc_radio25Cents.fill = GridBagConstraints.HORIZONTAL;
		gbc_radio25Cents.insets = new Insets(0, 0, 5, 0);
		gbc_radio25Cents.gridx = 0;
		gbc_radio25Cents.gridy = 3;
		panelConjTreinamento.add(radio25Cents, gbc_radio25Cents);
		radio25Cents.setName("3");
		groupCluster.add(radio25Cents);
		
		radio50Cents = new JRadioButton("50 centavos");
		GridBagConstraints gbc_radio50Cents = new GridBagConstraints();
		gbc_radio50Cents.fill = GridBagConstraints.HORIZONTAL;
		gbc_radio50Cents.insets = new Insets(0, 0, 5, 0);
		gbc_radio50Cents.gridx = 0;
		gbc_radio50Cents.gridy = 4;
		panelConjTreinamento.add(radio50Cents, gbc_radio50Cents);
		radio50Cents.setName("4");
		groupCluster.add(radio50Cents);
		
		radio1Real = new JRadioButton("1 real");
		GridBagConstraints gbc_radio1Real = new GridBagConstraints();
		gbc_radio1Real.fill = GridBagConstraints.HORIZONTAL;
		gbc_radio1Real.insets = new Insets(0, 0, 5, 0);
		gbc_radio1Real.gridx = 0;
		gbc_radio1Real.gridy = 5;
		panelConjTreinamento.add(radio1Real, gbc_radio1Real);
		radio1Real.setName("5");
		groupCluster.add(radio1Real);
		
		btnNewButton = new JButton("Salvar");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 6;
		panelConjTreinamento.add(btnNewButton, gbc_btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					save();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		
		panelTeste = new JPanel();
		panelTeste.setBorder(new TitledBorder(null, "Teste", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		controlPanel.add(panelTeste);
		GridBagLayout gbl_panelTeste = new GridBagLayout();
		gbl_panelTeste.columnWidths = new int[]{104, 0};
		gbl_panelTeste.rowHeights = new int[]{90, 19, 0};
		gbl_panelTeste.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelTeste.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panelTeste.setLayout(gbl_panelTeste);
		previewCanvas = new ImageCanvas();
		FlowLayout flowLayout = (FlowLayout) previewCanvas.getLayout();
		flowLayout.setVgap(2);
		flowLayout.setHgap(2);
		flowLayout.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_previewCanvas = new GridBagConstraints();
		gbc_previewCanvas.insets = new Insets(0, 0, 5, 0);
		gbc_previewCanvas.gridx = 0;
		gbc_previewCanvas.gridy = 0;
		panelTeste.add(previewCanvas, gbc_previewCanvas);
		previewCanvas.setMinimumSize(new Dimension(90, 90));
		previewCanvas.setPreferredSize(new Dimension(90, 90));
		previewCanvas.setMaximumSize(new Dimension(100, 100));
		previewCanvas.setSize(Constants.PREVIEW_DIAMETER, Constants.PREVIEW_DIAMETER);
		previewCanvas.setAlignmentX(Component.LEFT_ALIGNMENT);
		previewCanvas.setVisible(true);
		
		labelCluster = new JLabel("?");
		labelCluster.setAlignmentX(Component.CENTER_ALIGNMENT);
		GridBagConstraints gbc_labelCluster = new GridBagConstraints();
		gbc_labelCluster.gridx = 0;
		gbc_labelCluster.gridy = 1;
		panelTeste.add(labelCluster, gbc_labelCluster);
		labelCluster.setHorizontalAlignment(SwingConstants.CENTER);
		labelCluster.setFont(new Font("Dialog", Font.BOLD, 16));
		
		panelTraining = new JPanel();
		panelTraining.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Params. Treinamento", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		controlPanel.add(panelTraining);
		GridBagLayout gbl_panelTraining = new GridBagLayout();
		gbl_panelTraining.columnWidths = new int[] {0, 0, 0};
		gbl_panelTraining.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panelTraining.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panelTraining.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelTraining.setLayout(gbl_panelTraining);
		
		lblNewLabel = new JLabel("α1:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panelTraining.add(lblNewLabel, gbc_lblNewLabel);
		
		spinAlpha1 = new JSpinner();
		spinAlpha1.setModel(new SpinnerNumberModel(new Float(0.1f), null, null, new Float(0.001f)));
		GridBagConstraints gbc_spinAlpha1 = new GridBagConstraints();
		gbc_spinAlpha1.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinAlpha1.insets = new Insets(0, 0, 5, 0);
		gbc_spinAlpha1.gridx = 1;
		gbc_spinAlpha1.gridy = 0;
		panelTraining.add(spinAlpha1, gbc_spinAlpha1);
		
		label = new JLabel("α2:");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 1;
		panelTraining.add(label, gbc_label);
		
		spinAlpha2 = new JSpinner();
		spinAlpha2.setModel(new SpinnerNumberModel(new Float(0.001f), null, null, new Float(0.001f)));
		GridBagConstraints gbc_spinAlpha2 = new GridBagConstraints();
		gbc_spinAlpha2.insets = new Insets(0, 0, 5, 0);
		gbc_spinAlpha2.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinAlpha2.gridx = 1;
		gbc_spinAlpha2.gridy = 1;
		panelTraining.add(spinAlpha2, gbc_spinAlpha2);
		
		lblEpocas = new JLabel("Épocas:");
		lblEpocas.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblEpocas = new GridBagConstraints();
		gbc_lblEpocas.insets = new Insets(0, 0, 5, 5);
		gbc_lblEpocas.gridx = 0;
		gbc_lblEpocas.gridy = 2;
		panelTraining.add(lblEpocas, gbc_lblEpocas);
		
		spinEpochs = new JSpinner();
		spinEpochs.setModel(new SpinnerNumberModel(new Integer(10), null, null, new Integer(1)));
		GridBagConstraints gbc_spinEpochs = new GridBagConstraints();
		gbc_spinEpochs.insets = new Insets(0, 0, 5, 0);
		gbc_spinEpochs.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinEpochs.gridx = 1;
		gbc_spinEpochs.gridy = 2;
		panelTraining.add(spinEpochs, gbc_spinEpochs);
		
		btnTreinar = new JButton("Treinar");
		btnTreinar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				train();
			}
		});
		GridBagConstraints gbc_btnTreinar = new GridBagConstraints();
		gbc_btnTreinar.insets = new Insets(0, 0, 5, 0);
		gbc_btnTreinar.gridwidth = 2;
		gbc_btnTreinar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnTreinar.gridx = 0;
		gbc_btnTreinar.gridy = 3;
		panelTraining.add(btnTreinar, gbc_btnTreinar);
		
		lblErros = new JLabel("Erros:");
		GridBagConstraints gbc_lblErros = new GridBagConstraints();
		gbc_lblErros.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblErros.insets = new Insets(0, 0, 0, 5);
		gbc_lblErros.gridx = 0;
		gbc_lblErros.gridy = 4;
		panelTraining.add(lblErros, gbc_lblErros);
		
		lblErrorCount = new JLabel("?");
		GridBagConstraints gbc_lblErrorCount = new GridBagConstraints();
		gbc_lblErrorCount.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblErrorCount.gridx = 1;
		gbc_lblErrorCount.gridy = 4;
		panelTraining.add(lblErrorCount, gbc_lblErrorCount);
		
		try {
			configCore();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}