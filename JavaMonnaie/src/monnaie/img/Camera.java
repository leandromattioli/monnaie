package monnaie.img;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import monnaie.utils.Constants;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

public class Camera {
    //Video Capture Source
    private VideoCapture cam;
    
    //Frames
    private Mat currFrame = new Mat();
    private Mat yCrCbFrame = new Mat();
    private Mat normalizedFrame = new Mat();
    private Mat grayFrame = new Mat();
    private Mat thresholdFrame = new Mat();
    private Mat circleBGR = new Mat();
    private Mat circleYCrCb = new Mat();
    private Mat contoursFrame = new Mat();
    
    //Temp structures
    private Mat circleList = new Mat();
    private MatOfByte procFrameBytes = new MatOfByte();
    private List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
    
    //Coordinates
    private Point center = new Point();
    private int radius;
    
    //Sizes
    private boolean busy;
    
    public Camera() {
        this.busy = false;
        cam = new VideoCapture(Constants.CAM_ID);
        if(!cam.isOpened()) {
            System.out.format("Camera %d não foi aberta.", Constants.CAM_ID);
        }
        cam.set(Highgui.CV_CAP_PROP_FRAME_WIDTH, Constants.WIDTH);
        cam.set(Highgui.CV_CAP_PROP_FRAME_HEIGHT, Constants.HEIGHT);
        cam.set(Constants.CV_CAP_PROP_BRIGHTNESS, 0.5);
        cam.set(Constants.CV_CAP_PROP_CONTRAST, 1);
    }
    
    
    public void grabFrame() {
	if (!cam.grab())
            return;
        cam.retrieve(currFrame, Highgui.CV_CAP_ANDROID_COLOR_FRAME_RGB);
    }
    
    
    public void preProcess() {
    	//Equalize histogram
    	ArrayList<Mat> channels = new ArrayList<>();
    	Imgproc.cvtColor(currFrame, yCrCbFrame, Imgproc.COLOR_RGB2YCrCb);
    	Core.split(yCrCbFrame, channels);
    	Imgproc.equalizeHist(channels.get(0), channels.get(0));
        Core.merge(channels,yCrCbFrame);
        Imgproc.cvtColor(yCrCbFrame, normalizedFrame,Imgproc.COLOR_YCrCb2RGB);
        
        //Generating Gray Frame
    	Imgproc.cvtColor(normalizedFrame, grayFrame, Imgproc.COLOR_RGB2GRAY);
        //Imgproc.cvtColor(currFrame, grayFrame, Imgproc.COLOR_RGB2GRAY);
        Imgproc.GaussianBlur(grayFrame, grayFrame, Constants.gaussianSize, 2, 2);
        
        //Generating Thresholded Image
        //Imgproc.threshold(grayFrame, thresholdFrame, 30, 255, Imgproc.THRESH_BINARY_INV);
        Imgproc.adaptiveThreshold(grayFrame, thresholdFrame, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, 
        		Imgproc.THRESH_BINARY_INV, 31, 11);
        //Finding contours
        contours.clear();
        Imgproc.findContours(thresholdFrame.clone(), contours, new Mat(), 
        					 Imgproc.RETR_LIST,	Imgproc.CHAIN_APPROX_SIMPLE);
        contoursFrame = currFrame.clone();
    	Imgproc.drawContours(contoursFrame, contours, -1, Constants.contourColor);        
    }
    
    
    public boolean detectCircles() {
    	boolean found = false;
        busy = true;
        Rect r;
        //Imgproc.equalizeHist(grayFrame, grayFrame); //normalized já é equalizada
        //Imgproc.threshold(grayFrame, grayFrame, 90, 255, Imgproc.THRESH_BINARY);
        //Imgproc.HoughCircles(grayFrame, circles, Imgproc.CV_HOUGH_GRADIENT, 
        //                     100, 30, 200, 100, 30, 35);
        Imgproc.HoughCircles(grayFrame, circleList, Imgproc.CV_HOUGH_GRADIENT, 
                             2, 300, 200, 100, 40, 0);
        if(circleList.total() > 0) {
            center.x = (int) Math.round(circleList.get(0, 0)[0]);
            center.y = (int) Math.round(circleList.get(0, 0)[1]);
            radius = (int) Math.round(circleList.get(0, 0)[2]);
            //Core.circle(currFrame, center, radius, new Scalar(0,0,200), 3, 8, 0);
            //Surrounding rectangle
            Point p1 = new Point();
            p1.x = center.x - radius;
            p1.y = center.y - radius;
            Point p2 = new Point();
            p2.x = p1.x + 2*radius;
            p2.y = p1.y + 2*radius;
            r = new Rect(p1, p2);
            if(p1.x > 0 && p2.x > 0 && p1.y > 0 && p2.y > 0) {
            	try {
            		ArrayList<Mat> channels = new ArrayList<Mat>();
            		circleYCrCb = new Mat(currFrame, r).clone();
            		Imgproc.cvtColor(circleYCrCb, circleYCrCb, Imgproc.COLOR_BGR2YCrCb);
            		Core.split(circleYCrCb, channels);
                	Imgproc.equalizeHist(channels.get(0), channels.get(0));
                    Core.merge(channels,circleYCrCb);
                    Imgproc.cvtColor(circleYCrCb, circleBGR,Imgproc.COLOR_YCrCb2BGR);
            		found = true;
            	}
            	catch(Exception e) { 
            		System.out.println("Erro no frame!");
        		}
            }
        }
        busy = false;
        return found;
    }
    
    
    public boolean isBusy() {
        return busy;
    }
    
    public void showValue(short cluster) {
    	String message = String.format("R$ %.2f", Constants.clusterMap[cluster]);
    	Core.putText(currFrame, message, new Point(center.x, center.y), 
    			     Core.FONT_HERSHEY_SIMPLEX, 1, new Scalar(255,0,0));
    }
    
    // Frames accessors
    
    public BufferedImage getImage() throws IOException {
    	return getImgBuffer(currFrame);
    }
    
    /** @OBSOLETE **/
    public BufferedImage getNormalizedImage() throws IOException {
    	if(normalizedFrame.empty())
    		return null;
    	return getImgBuffer(normalizedFrame);
    }
    
    public BufferedImage getGrayImage() throws IOException {
    	return getImgBuffer(grayFrame);
    }
    
    public BufferedImage getThresholdedImage()  throws IOException {
    	return getImgBuffer(thresholdFrame);
    } 
    
    public BufferedImage getContoursImage() throws IOException {
    	return getImgBuffer(contoursFrame);
    }
    
    public BufferedImage getCircleBGR() throws IOException {
    	return getImgBuffer(circleBGR);
    }
    
    public BufferedImage getCircleBGR(Size circleSize) throws IOException {
        if(circleBGR.empty())
            return null;
        Mat resizedCircle = new Mat();
        Imgproc.resize(circleBGR, resizedCircle, circleSize);
        return getImgBuffer(resizedCircle);
    }
    
    
    public Mat getCircleBGRMat(Size circleSize) {
    	Mat resizedCircle = new Mat();
    	Imgproc.resize(circleBGR, resizedCircle, circleSize);
    	return resizedCircle;
    }
    
    public Mat getCircleBGRMat() {
    	return circleBGR;
    }
    
    public BufferedImage getCircleYCrCb() throws IOException {
    	return getImgBuffer(circleYCrCb);
    }
    
    public BufferedImage getCircleYCrCb(Size circleSize) throws IOException {
        if(circleYCrCb.empty())
            return null;
        Mat resizedCircle = new Mat();
        Imgproc.resize(circleYCrCb, resizedCircle, circleSize);
        return getImgBuffer(resizedCircle);
    }
    
    
    public Mat getCircleYCrCbMat(Size circleSize) {
    	Mat resizedCircle = new Mat();
    	Imgproc.resize(circleYCrCb, resizedCircle, circleSize);
    	return resizedCircle;
    }
    
    public Mat getCircleYCrCbMat() {
    	return circleYCrCb;
    }
    
    //Private methods
    
    private BufferedImage getImgBuffer(Mat image) throws IOException {
        BufferedImage bufImage;
        Highgui.imencode(".bmp", image, procFrameBytes);
        byte[] byteArray = procFrameBytes.toArray();
        InputStream in = new ByteArrayInputStream(byteArray);
        bufImage = ImageIO.read(in);
        return bufImage;
    }
}