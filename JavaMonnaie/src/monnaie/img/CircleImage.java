package monnaie.img;

import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

//Circle image in YCrCb

public class CircleImage {
	private Mat img;
	
	public CircleImage(Mat img) {
		this.img = img;
	}
	
	public CircleImage(String filePath) {
		this.img = Highgui.imread(filePath);
		Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2YCrCb);
	}
	
	public byte[] getAvgColor(float minRadius, float maxRadius) {
		CircleIterator iter = new CircleIterator(this.img, minRadius, maxRadius);
		byte[] pixel;
		int[] avgColorTmp = new int[] {0, 0, 0};
		byte[] avgColor = new byte[3];
		int count = iter.count();
		while(iter.hasNext()) {
			pixel = iter.next();
			avgColorTmp[0] += pixel[0];
			avgColorTmp[1] += pixel[1];
			avgColorTmp[2] += pixel[2];
		}
		avgColorTmp[0] /= count;
		avgColorTmp[1] /= count;
		avgColorTmp[2] /= count;
		avgColor[0] = (byte) (avgColorTmp[0]);
		avgColor[1] = (byte) (avgColorTmp[1]);
		avgColor[2] = (byte) (avgColorTmp[2]);
		return avgColor;
	}
	
	public ArrayList<Byte> getFeatures() {
		ArrayList<Byte> features = new ArrayList<>(3);
    	
		byte[] avgColor = getAvgColor(0, 1);
		byte[] avgExtRad = getAvgColor(0.75f, 0.9f);
		byte[] avgIntRad = getAvgColor(0, 0.5f);
		
		//Feature 1 : Mean color (Cr and Cb) 
		//Feature 2 : Color difference between outer radius and inner radius    	
    	//Feature 3 : Diameter (image width)
    	
    	//Creating Feature Vector
    	features.add(avgColor[1]);
    	features.add(avgColor[2]);
    	features.add( (byte) (avgExtRad[1] - avgIntRad[1]) );
    	features.add( (byte) (avgExtRad[2] - avgIntRad[2]) );
    	features.add((byte) img.width());
    	
    	return features;
	}
}
