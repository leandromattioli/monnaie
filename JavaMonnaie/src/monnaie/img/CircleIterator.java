package monnaie.img;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import monnaie.lvq.Point;

import org.opencv.core.Mat;

public class CircleIterator implements Iterator<byte[]> {
	
	private int currPoint;
	private ArrayList<Point> points;
	private Mat matImg;
	private BufferedImage bufImg;
	private float minNormRadius;
	private float maxNormRadius;
    
    public CircleIterator(Mat img) {
    	this.minNormRadius = 0;
    	this.maxNormRadius = 1;
    	this.matImg = img;
    	config(img.height(), img.width(), img.rows(), img.cols());
    }
    
    public CircleIterator(BufferedImage img) {
    	this.minNormRadius = 0;
    	this.maxNormRadius = 1;
    	this.bufImg = img;
		config(img.getHeight(), img.getWidth(), bufImg.getHeight(), bufImg.getWidth());
    }
    
    public CircleIterator(Mat img, float minNormRadius, float maxNormRadius) {
    	this.minNormRadius = minNormRadius;
    	this.maxNormRadius = maxNormRadius;
    	this.matImg = img;
    	config(img.height(), img.width(), img.rows(), img.cols());
    }
    
    public CircleIterator(BufferedImage img, float minNormRadius, float maxNormRadius) {
    	this.minNormRadius = minNormRadius;
    	this.maxNormRadius = maxNormRadius;
    	this.bufImg = img;
		config(img.getHeight(), img.getWidth(), bufImg.getHeight(), bufImg.getWidth());
    }
    
    private void config(int height, int width, int rows, int cols) {
		int radius = width / 2;
		int xc = width / 2;
		int yc = height / 2;
		points = new ArrayList<>();
		float r;
		for(int y = 0; y < rows; y++)
			for(int x = 0; x < cols; x++) {
				r = (float) (Math.pow(x - xc, 2) + Math.pow(y - yc, 2)); 
                if(r <= Math.pow(radius * this.maxNormRadius, 2) && 
                   r >= Math.pow(radius * this.minNormRadius, 2))
                	points.add(new Point(x, y));
			}
		currPoint = 0;
    }
        
    public int count() {
    	return points.size();
    }
    
    @Override
    /**
     * @NOTE Valor dos pixels: 0x00 --> -128 e 0xFF --> 127 (não é complemento de 2)
     */
    public byte[] next() {
    	int x, y;
    	byte[] pixel;
    	double[] pixelDbl;
    	int color;
    	if(currPoint < points.size()) {
    		x = points.get(currPoint).x;
    		y = points.get(currPoint).y;
    		pixel = new byte[3];
    		if(matImg != null) {
    			pixelDbl = matImg.get(y, x);
	    		pixel[0] = (byte) (pixelDbl[0] - 128);	//b
	    		pixel[1] = (byte) (pixelDbl[1] - 128);	//g
	    		pixel[2] = (byte) (pixelDbl[2] - 128);	//r
    		}
    		else {
				color = bufImg.getRGB(x, y);
				pixel[0] = (byte) ((color & 0x000000ff) - 128);			//blue
				pixel[1] = (byte) (((color & 0x0000ff00) >> 8) - 128);  //green
				pixel[2] = (byte) (((color & 0x00ff0000) >> 16) - 128); //red
    		}
            currPoint++;
            return pixel;
    	}
        else
            throw new NoSuchElementException();
    }
    
    
	@Override
	public boolean hasNext() {
		return currPoint < points.size();
	}
	
	
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}