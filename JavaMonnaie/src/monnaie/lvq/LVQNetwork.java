package monnaie.lvq;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import monnaie.img.CircleImage;
import monnaie.utils.Log;
import monnaie.utils.Constants;
import monnaie.utils.U;

public class LVQNetwork {
	float[][] weights;
	short nInputs;
	short nOutputs;
	
	public LVQNetwork(short nInputs, short nOutputs) {
		this.nInputs = nInputs;
		this.nOutputs = nOutputs;
		weights = new float[nInputs][nOutputs];
		//Log
		Log.configLog("NETWORK");
	}
	
	public void initWeights() {
		ArrayList<Byte> features;
		CircleImage circle;
		for(int j = 1; j < nOutputs; j++) { //ignoring cluster 0
			circle = new CircleImage(Constants.IMG_WEIGHT_PREFIX + "_" + j + ".bmp");
			features = circle.getFeatures();
			//Log
			String msg = String.format("%d : %s", j, U.fmtByteTuple(features));
			Log.i("NETWORK", msg);
			for(int i = 0; i < nInputs; i++)
				weights[i][j] = features.get(i);
		}
	}
	
	public short classify(ArrayList<Byte> input) {
		float minDist = 0;
		float currDist;
		short cluster = 1; //ignoring cluster 0
		
		//Iterating clusters
		for(short j = 1; j < nOutputs; j++) { //ignoring cluster 0
			currDist = 0;
			//Iterating inputs
			for(int i = 0; i < nInputs; i++)
				currDist += (float) Math.pow(weights[i][j] - (float) (input.get(i)), 2);
			//Finding minimal distance
			if(j == 1) //ignoring cluster 0
				minDist = currDist;
			else if(currDist < minDist) {
				minDist = currDist;
				cluster = j;
			}
		}
		return cluster;
	}
	
	
	//Persistence methods
	
	public void toBinFile(String filePath) throws IOException {
		RandomAccessFile out = new RandomAccessFile(filePath, "rw");
		out.writeShort(nInputs);
		out.writeShort(nOutputs);
		for(int i = 0; i < nInputs; i++) {
			for(int j = 0; j < nOutputs; j++)
				out.writeFloat(weights[i][j]);
		}
		out.close();
	}
	
	public void fromBinFile(String filePath) throws IOException {
		RandomAccessFile in = new RandomAccessFile(filePath, "r");
		short nInputs = in.readShort();
		short nOutputs = in.readShort();
		if(nInputs != this.nInputs || nOutputs != this.nOutputs) {
			in.close();
			throw new IOException("Invalid file!");
		}
		for(int i = 0; i < nInputs; i++) {
			for(int j = 0; j < nOutputs; j++)
				weights[i][j] = in.readFloat();
		}
		in.close();
	}
	
	
	public void toCSVFile(String filePath) {
		PrintWriter out;
		try {
			out = new PrintWriter(filePath);
			for(int i = 0; i < nInputs; i++) {
				for(int j = 0; j < nOutputs; j++) {
					out.print(String.format("%.4f;", weights[i][j]));
				}
				out.println();
				out.flush();
			}
			out.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	
	public void fromCSVFile(String filePath) throws IOException {
		FileReader reader = new FileReader(filePath);
		BufferedReader buf = new BufferedReader(reader);
		String line = buf.readLine();
		String[] parts;
		int i = 0;
		while(line != null) {
			parts = line.split(";");
			for(int j = 0; j < parts.length; j++)
				weights[i][j] = Float.parseFloat(parts[j]);
			line = buf.readLine();
			i++;
		}
		buf.close();
	}

}
