package monnaie.lvq;

import java.io.File;
import java.util.ArrayList;

import monnaie.img.CircleImage;
import monnaie.utils.Constants;
import monnaie.utils.Log;
import monnaie.utils.U;

public class LVQTrainer {
	
	//Write-only Properties
	
	private LVQNetwork net;
	private float initialAlpha;
	private float finalAlpha;
	private int epochs;
	
	//Read-only Properties
	private int wrong;

	// Public Methods
	
	public LVQTrainer() {
		Log.configLog("TRAINER");
	}
	
	public void train(PatternTable trainingSet) {
		float alpha = initialAlpha;
		float alphaStep = (initialAlpha - finalAlpha) / (float) (epochs - 1);
		//Step 0 : Done outside this method
		//Step 1 : While stopping condition is false...
		for(int currEpoch = 0; currEpoch < epochs; currEpoch++) {
			//Step 2 : For each training input vector x...
			ArrayList<Byte> xs;
			int j;
			int targetCluster;
			for(int t = 0; t < trainingSet.size(); t++) {
				xs = trainingSet.getInputs(t);
				targetCluster = trainingSet.getCluster(t);
				//Step 3 : Find J so that | x - wj | is minimum
				j = net.classify(xs);
				//Step 4 : Update wj
				if(j == targetCluster) {
					for(int i = 0; i < xs.size(); i++)
						net.weights[i][j] += alpha * ((float) xs.get(i) - net.weights[i][j]);
				}
				else {
					for(int i = 0; i < xs.size(); i++)
						net.weights[i][j] -= alpha * ((float) xs.get(i) - net.weights[i][j]);
				}
			}
			//Step 5 : Reduce learning rate
			alpha -= alphaStep;
			//Step 6 : Stop criteria by number of epochs
		}
	}
	
	public PatternTable generateFeaturesTrainingSet() {
		return createPatternTable(Constants.IMG_TRAINING_DIR);
	}
	
	public PatternTable generateFeaturesTestSet() {
		return createPatternTable(Constants.IMG_TEST_DIR);
	}
	
	public void test(PatternTable trainingSet) {
		wrong = 0;
		for(int i = 0; i < trainingSet.size(); i++) {
			ArrayList<Byte> input = trainingSet.getInputs(i);
			short desiredCluster = trainingSet.getCluster(i).shortValue();
			short currCluster = net.classify(input); 
			if(desiredCluster != currCluster) {
				Log.i("TRAINER", String.format("%d instead of %d", currCluster, desiredCluster));
				wrong++;
			}
		}
		Log.i("TRAINER", String.format("%d", wrong));
	}
	
	// Private methods
	
	private PatternTable createPatternTable(String path) {
		PatternTable pat = new PatternTable();
		File dir = new File(path);
		File files[] = dir.listFiles();
		if(files == null)
			return pat;
		String fileName;
		String nameParts[];
		int cluster;
		CircleImage circle;
		for(int i = 0; i < files.length; i++) {
			fileName = files[i].getName();
			nameParts = fileName.split("_");
			cluster = Integer.parseInt(nameParts[1]);
			circle = new CircleImage(files[i].getAbsolutePath());
			ArrayList<Byte> features = circle.getFeatures();
			pat.addEntry(features, cluster);
			//Log
			String msg = fileName + " : " + U.fmtByteTuple(features);
			Log.i("TRAINER", msg);
		}
		return pat;
	}
	

	//Accessor methods
	
	public void setNetwork(LVQNetwork network) {
		this.net = network;
	}

	public void setInitialAlpha(float initialAlpha) {
		this.initialAlpha = initialAlpha;
	}

	public void setFinalAlpha(float finalAlpha) {
		this.finalAlpha = finalAlpha;
	}

	public void setEpochs(int epochs) {
		this.epochs = epochs;
	}

	public int getWrongCount() {
		return wrong;
	}
}
