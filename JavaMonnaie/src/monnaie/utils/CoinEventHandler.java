package monnaie.utils;

public class CoinEventHandler {
	private short[] lastClusters;
	private short coinCounter = 0;
	private short noCoinCounter = 0;
	private float money = 0f;
	private boolean locked = false;
	
	public CoinEventHandler() {
		lastClusters = new short[15];
	}
	
	public void addCoinEvent(short cluster) {
		if(!locked) {
			lastClusters[coinCounter] = cluster;
			coinCounter++;
			if(coinCounter == 15) {
				coinCounter = 0;
				money += getMostPopularValue();
				locked = true;
			}
		}
	}
	
	public void addNoneEvent() {
		if(locked) {
			noCoinCounter++;
			if(noCoinCounter == 10) {
				noCoinCounter = 0;
				locked = false;
			}
		}
	}
	
	public float getMoney() {
		return money;
	}
	
	public boolean isLocked() {
		return locked;
	}
	
	private float getMostPopularValue() {
		int i;
		short[] found = {0, 0, 0, 0, 0, 0};

		//Evaluating frequencies
		for(i = 0; i < lastClusters.length; i++)
			found[lastClusters[i]]++;
		
		//Finding max
		int maxIdx = 0;
		short maxVal = found[0];
		for(i = 0; i < found.length; i++)
			if(found[i] > maxVal) {
				maxVal = found[i];
				maxIdx = i;
			}
		return Constants.clusterMap[maxIdx];
	}
}
