package monnaie.utils;

import java.text.SimpleDateFormat;

import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;

public class Constants {
	public static final short DIAMETER = 40;
	public static final short PREVIEW_DIAMETER = 90;
	public static final short PIXELS = 1254;
	public static final short CLASSES = 6;
    public static final int CAM_ID = 1;
    public static final int WIDTH = 320;
    public static final int HEIGHT = 240;
    public static final float RADIUS_SEP = 0.75f;
    public static final int CV_CAP_PROP_BRIGHTNESS = 10;
    public static final int CV_CAP_PROP_CONTRAST = 11;
    
	public static final String IMG_WEIGHT_PREFIX = "img/init/cluster";
	public static final String IMG_TRAINING_DIR = "img/train";
	public static final String IMG_TRAINING_PREFIX = "img/train/cluster";
	public static final String IMG_TEST_DIR = "img/test";
	public static final String IMG_TEST_PREFIX = "img/test/cluster";
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyyMMdd_hhmmss");
    public static final Scalar contourColor = new Scalar(255, 0, 0);
    public static final Size gaussianSize = new Size(7,7);
    public static final Point textOffset = new Point(30, 90);
    public static final float[] clusterMap = {0.01f, 0.05f, 0.10f, 0.25f, 0.50f, 1.00f};
    
}