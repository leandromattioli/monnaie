package monnaie.utils;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;

public class Log {
	private static ConsoleHandler consoleHandler = new ConsoleHandler();
	
	public static void configLog(String name) {
		Logger log = Logger.getLogger(name);
		log.addHandler(consoleHandler);
		FileHandler fh;
		try {
			fh = new FileHandler("log/" + name + ".xml");
			log.addHandler(fh);
		} 
		catch (Exception e) {
			System.err.println("Cannot add log " + name + "!");
		}
	}
	
	public static void i(String name, String msg) {
		Logger log = Logger.getLogger(name);
		log.log(Level.INFO, msg);
	}
}
