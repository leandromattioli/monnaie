package monnaie.utils;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import monnaie.img.CircleImage;
import monnaie.utils.Constants;
import monnaie.utils.U;

public class Test {
	static { System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

	public static void main(String[] args) {
		test3();
	}
	
	public static void test1() {
		CircleImage img = new CircleImage("img/test/test.bmp");
		byte[] avgColor = img.getAvgColor(0, 1f);
		System.out.println(U.fmtByteTuple(avgColor));
		System.exit(0);
	}

	public static void test2() {
		CircleImage img = new CircleImage("img/init/cluster_1.bmp");
		byte[] avgColor = img.getAvgColor(0, 1f);
		System.out.println(U.fmtByteTuple(avgColor));
		ArrayList<Byte> features = img.getFeatures();
		System.out.println(U.fmtByteTuple(features));
		Mat img1 = Highgui.imread(Constants.IMG_WEIGHT_PREFIX + "_" + '1' + ".bmp");
		CircleImage circle = new CircleImage(img1);
		features = circle.getFeatures();
		System.out.println(U.fmtByteTuple(features));
		System.exit(0);
	}
	
	public static void test3() {
		CircleImage img = new CircleImage("img/init/cluster_5.bmp");
		System.out.println("Raio externo");
		System.out.println(U.fmtByteTuple(img.getAvgColor(0.9f, 1f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0.8f, 1f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0.75f, 1f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0.75f, 0.95f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0.75f, 0.9f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0.8f, 0.9f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0.8f, 0.95f)));
		System.out.println("Raio interno");
		System.out.println(U.fmtByteTuple(img.getAvgColor(0, 0.4f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0, 0.5f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0, 0.6f)));
		System.out.println(U.fmtByteTuple(img.getAvgColor(0, 0.75f)));
		System.exit(0);
	}
	
	
}
