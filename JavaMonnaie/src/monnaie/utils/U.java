package monnaie.utils;

import java.util.ArrayList;

public class U {
	public static String fmtUByte(Byte val) {
		int unsigned = val.intValue() & 0xFF;
		return String.format("%d", unsigned);
	}
	
	public static String fmtByte(Byte val) {
		return String.format("%d", val);
	}
	
	public static String fmtByteTuple(byte[] array) {
		String msg = "";
		for(int i = 0; i < array.length; i++) {
			msg += U.fmtByte(array[i]) + " ";
		}
		return msg;
	}
	
	public static String fmtByteTuple(ArrayList<Byte> array) {
		byte[] arr = new byte[array.size()];
		for(int i = 0; i < array.size(); i++)
			arr[i] = array.get(i);
		return U.fmtByteTuple(arr);
	}
	
}
