/** 
 * @file CircleFeatures.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 10 de Novembro de 2016, 10:40
 */

#include <opencv2/core/core.hpp>
#include <cstdint>
#include <array>

#ifdef __ARM_NEON
#include <arm_neon.h>
#endif

#ifndef CIRCLEFEATURES_H
#define CIRCLEFEATURES_H

#define F_LOWER_CR 100
#define F_UPPER_CR 200
#define F_LOWER_CB 50
#define F_UPPER_CB 150
#define F_LOWER_RAD 30
#define F_UPPER_RAD 100

/// Circle features (average color, radius and color variation)

class CircleFeatures {
public:
    ///Constructor
    CircleFeatures();
    
    /// Circle average color (Cr component)
    uint8_t avgCr;

    /// Circle average color (Cb component)
    uint8_t avgCb;

    /** 
     * @brief Circle average color variation (Cr component)
     * 
     * The color variation is measured between the circle 
     * \f$\rho \leq 0.5 \cdot r\f$ and the annulus 
     * \f$ 0.75 \cdot r \leq \rho \leq 0.9 \cdot r\f$, where 
     * \f$r\f$ is the circle radius and \f$\rho\f$ is the pixel's distance
     * from the circle center
     */
    uint8_t deltaAvgCr;

    /** 
     * @brief Circle average color variation (Cb component)
     * 
     * The color variation is measured between the circle 
     * \f$\rho \leq 0.5 \cdot r\f$ and the annulus 
     * \f$ 0.75 \cdot r \leq \rho \leq 0.9 \cdot r\f$, where 
     * \f$r\f$ is the circle radius and \f$\rho\f$ is the pixel's distance
     * from the circle center*/
    uint8_t deltaAvgCb;

    /// Circle radius
    uint8_t radius;

    /**
     * Converts the structure to an std::array
     * @return A std::array of 8-bit unsigned values, ordered as below:
     * Elements 0 and 1 - average color for components Cr and Cb
     * Element 2 - radius
     * Elements 3 and 4 - color variation (circle and annulus; Cr and Cb)
     */
    std::array<uint8_t, 5> toStdArray() const;
    
    /**
     * Creates a vector from normalized parameters Cr, Cb and radius
     * @return A vector of 3 floating point numbers, ordered as below:
     * Elements 0 and 1 - average color for components Cr and Cb
     * Element 2 - radius
     */
    std::vector<float> toLVQInput() const;
    
#ifdef NEON_NORMALIZE
    float32x4_t lower = {F_LOWER_CR, F_LOWER_CB, F_LOWER_RAD};
    float32x4_t upper = {F_UPPER_CR, F_UPPER_CB, F_UPPER_RAD};
    float32x4_t deltaInv;
#endif
    
};

#endif /* CIRCLEFEATURES_H */

