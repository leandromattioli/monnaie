/** 
 * @file CircleImage.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 7 de Novembro de 2016, 09:38
 */

#ifndef CIRCLEIMAGE_H
#define CIRCLEIMAGE_H

#include <cstddef>
#include <cstdlib>
#include <cstdint>
#include <string>
#include <opencv2/core/core.hpp>

#include "CircleFeatures.h"
#include "PatternTable.h"

/**
 * @brief Circle inscribed in a rectangle
 * @param img A color image in YCrCb format
 */
class CircleImage {
public:
    /**
     * Constructor
     * @param img A color image in YCrCb format with an inscribed circle 
     */
    CircleImage(cv::Mat img);


    /**
     * Alternative constructor
     * @param img A color image in YCrCb format
     * @param xc Circle center X coordinate
     * @param yc Circle center Y coordinate
     * @param radius Circle radius
     */
    CircleImage(cv::Mat img, int16_t xc, int16_t yc, uint16_t radius);


    /**
     * Alternative constructor
     * @param filePath Valid path to a color image with an inscribed circle
     */
    CircleImage(std::string filePath);

    virtual ~CircleImage();

    /**
     * Gets the average color inside the circle
     * @param minNormRadius Minimum normalized radius (defaults to 0.0)
     * @param maxNormRadius Maximum normalized radius (defaults to 1.0)
     * @return The average color
     */
    cv::Vec3b getAvgColor(float minNormRadius, float maxNormRadius);

    /**
     * Gets the features (LVQ inputs) from the circle image 
     * @return A structure containing the coin features.
     * @see CircleFeatures
     */
    CircleFeatures getFeatures();

    /**
     * Creates a pattern table from a list of images
     * @param images List of file paths corresponding to sample images
     * @return A Pattern Table constructed from the sample images
     * @note The cluster associated to each file is related to a naming 
     * convention, counting with the presence of one of the following strings:
     * moeda_005, moeda_010, moeda_025 and moeda_050
     */
    static PatternTable createPatternTable(std::vector<std::string> images);
    
    /**
     * Retrieves an input / output pair for a sample image
     * @param filename The file path to the image
     * @return A TableEntry computed from the image
     */
    static TableEntry getTableEntry(std::string filename);
    
    /**
     * Gets the cluster associated to an image file
     * @param filename The file path for a color 8-bit image
     * @return The corresponding cluster or -1 if no cluster is associated
     */
    static int8_t getCluster(std::string filename);

    /// Acessor method
    cv::Mat getImg() const;

private:
    /// OpenCV image
    cv::Mat img;

    /// Whether the image should be released from memory in the destructor
    bool shouldReleaseImage;

    ///@{  
    /// Circle center coordinates
    int16_t xc, yc;
    ///@}

    /// Circle radius
    uint16_t radius;

};

#endif /* CIRCLEIMAGE_H */