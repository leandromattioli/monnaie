/** 
 * @file Classifier.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 1 de Dezembro de 2016, 20:47
 */

#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <cstdint>

//Forward declarations
class CircleFeatures;
class LVQNetwork;

class Classifier {
public:
    Classifier(LVQNetwork& net);
    Classifier(const Classifier& orig);
    int8_t classify(CircleFeatures& features);
    virtual ~Classifier();
private:
    LVQNetwork& network;
    bool inRange(float val, float min, float max);
    bool inRange(int val, int min, int max);
};

#endif /* CLASSIFIER_H */

