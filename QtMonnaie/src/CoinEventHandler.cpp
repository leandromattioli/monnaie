/** 
 * @file CoinEventHandler.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 14 de Novembro de 2016, 01:28
 */

#include <cstdint>

#include "CoinEventHandler.h"

CoinEventHandler::CoinEventHandler() {
}

CoinEventHandler::~CoinEventHandler() {
}

void CoinEventHandler::addCoinEvent(uint8_t cluster) {
    if (!locked) {
        lastClusters[coinCounter] = cluster;
        coinCounter++;
        if (coinCounter == 15) {
            coinCounter = 0;
            money += getMostPopularValue();
            locked = true;
        }
    }
}

void CoinEventHandler::addNoneEvent() {
    if (locked) {
        noCoinCounter++;
        if (noCoinCounter == 10) {
            noCoinCounter = 0;
            locked = false;
        }
    }
}

float CoinEventHandler::getMoney() const {
    return money;
}

bool CoinEventHandler::isLocked() const {
    return locked;
}

float CoinEventHandler::getMostPopularValue() {
    uint8_t i;
    uint8_t found[] = {0, 0, 0, 0, 0, 0};

    //Evaluating frequencies
    for (i = 0; i < 15; i++)
        found[lastClusters[i]]++;

    //Finding max
    int maxIdx = 0;
    short maxVal = found[0];
    for (i = 0; i < 6; i++)
        if (found[i] > maxVal) {
            maxVal = found[i];
            maxIdx = i;
        }
    return clustersVal[maxIdx];
}





