/** 
 * @file Image.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 13 de Outubro de 2016, 20:33
 */

#include <QObject>
#include <QImage>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "Image.h"

Image::Image(Mat mat) {
    this->mat = mat;
}

Image::Image(QString& filename) {
    this->mat = imread(filename.toStdString(), IMREAD_COLOR);
    if(this->mat.data == NULL) {
        throw cv::Exception();
    }
}

QImage Image::makeQImage() const {
    if (mat.channels() == 3) {
        return QImage((const uint8_t*) (mat.data),
                mat.cols, mat.rows, QImage::Format_RGB888).rgbSwapped();
    } else {
        return QImage((const uint8_t*) (mat.data),
                mat.cols, mat.rows, QImage::Format_Indexed8);
    }
}

Mat Image::getMat() const {
    return mat;
};

Vec3b Image::getYCrCbColorAt(int x, int y) const {
    Vec3b bgr = getBGRColorAt(x, y);
    Mat3b bgrMat(bgr);
    Mat3b yCrCbMat;
    cvtColor(bgrMat, yCrCbMat, CV_BGR2YCrCb);
    return yCrCbMat.at<Vec3b>(0, 0);
}

Vec3b Image::getBGRColorAt(int x, int y) const {
    return this->mat.at<Vec3b>(y, x);
}

Scalar Image::getIntensityAt(int x, int y) const {
    return this->mat.at<uint8_t>(y, x);
}

void Image::load(QString& filename) {
    this->mat = imread(filename.toStdString(), IMREAD_COLOR);
    if(this->mat.data == NULL) {
        throw cv::Exception();
    }
}

bool Image::save(QString& filename) const {
    return imwrite(filename.toStdString(), this->mat);
}

bool Image::validCoords(int x, int y) const {
    return (x >= 0) && (y >= 0) && (x < this->mat.cols) && (y < this->mat.rows);
}



