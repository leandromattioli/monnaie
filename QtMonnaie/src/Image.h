/** 
 * @file Image.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date 13 de Outubro de 2016, 20:33
 */

#ifndef IMAGE_H
#define IMAGE_H

#include <QImage>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

///High-level image
class Image {
    
public:
    
    /**
     * Constructor
     * @param mat An OpenCV image
     */
    Image(Mat mat);
    
    /**
     * Alternative constructor: creates an Image object from a file
     * @param filename Image file path
     */
    Image(QString& filename);
    
    /**
     * Constructs a QImage from the OpenCV image
     * @return An image suited for Qt GUI usage
     */
    QImage makeQImage() const;
    
    /**
     * Gets the color at the position (x, y)
     * @param x Column index
     * @param y Row index
     * @return The color in BGR color space
     */
    Vec3b getBGRColorAt(int x, int y) const;
    
    /**
     * Gets the color at the position (x, y)
     * @param x Column index
     * @param y Row index
     * @return The color in YCrCb color space
     */
    Vec3b getYCrCbColorAt(int x, int y) const;
    
    /**
     * Gets the intensity (for grayscale images) at the position (x, y)
     * @param x Column index
     * @param y Row index
     * @return The intensity
     */    
    Scalar getIntensityAt(int x, int y) const;
    
    /**
     * Checks whether a point is inside the image
     * @param x Column index
     * @param y Row index
     * @return Whether the provided coordinates are inside the image
     */
    bool validCoords(int x, int y) const;
    
    /**
     * Loads the image from a file
     * @param filename Color Image file path
     */
    void load(QString& filename);
    
    /**
     * Saves the image to a file
     * @param filename Output file path
     * @return Whether the operation was successful
     */
    bool save(QString& filename) const;

    /// Accessor for the internal Mat element
    Mat getMat() const;
private:
    Mat mat;
};

#endif /* IMAGE_H */

