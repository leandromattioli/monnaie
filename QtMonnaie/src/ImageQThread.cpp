/** 
 * @file ImageQThread.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date 13 de Outubro de 2016, 20:36
 */

#include <opencv2/highgui/highgui.hpp>
#include <QThread>
#include <QImage>
#include "ImageQThread.h"
#include "ImageTask.h"


ImageQThread::ImageQThread(QObject *parent) :
QThread(parent) {
    (void) parent; //make compiler happy!
    stopFlag = true;
    task = nullptr;
}

ImageQThread::~ImageQThread() {
    stopFlag = true;
    if(task != nullptr)
        delete task;
    wait();
}

bool ImageQThread::isStopped() const {
    return this->stopFlag;
}

void ImageQThread::begin() {
    if (!isRunning()) {
        stopFlag = false;
        start(LowPriority);
    }
}

void ImageQThread::stop() {
    this->stopFlag = true;
}

void ImageQThread::run() {
    int frameRate = 30;
    int delay = 1000 / frameRate;
    ImageTask::FrameResult result;
    while (!stopFlag) {
        result = task->singleIteration();
        if(result == ImageTask::FrameResult::kNoImage)
            return;
        Mat bgrMat = task->getLastFrame();
        emit newFrame(bgrMat);
        if(result == ImageTask::FrameResult::kCoinDetected) {
            coin = task->getLastCoin();
            CircleFeatures features = task->getLastCoinFeatures();
            emit newCoin(coin, features);
        }
        else
            emit noCoin();
    }
}

void ImageQThread::initCapture(int deviceNo) {
    task = new ImageTask(deviceNo);
}




