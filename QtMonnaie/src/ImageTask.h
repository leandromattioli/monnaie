/** 
 * @file ImageTask.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date November 6, 2016, 20:18 AM
 */

#ifndef IMAGETASK_H
#define IMAGETASK_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "CircleImage.h"

using namespace cv;

/**
 * @brief OpenCV task
 * @param deviceNo The camera to use or -1 to use whatever is available
 */
class ImageTask {
public:
    
    /// Return result while processing a frame
    enum FrameResult {
        kNoImage,
        kNoCoin,
        kCoinDetected
    };
    
    /**
     * Constructor
     * @param deviceNo The camera to use or -1 to use whatever is available
     */
    ImageTask(int deviceNo);
    
    virtual ~ImageTask();
    
    /// Task main iteration (acquire, pre-process, feature extraction)
    FrameResult singleIteration();
    
    /// Retrieves the last frame acquired from the Camera
    Mat getLastFrame() const;
    
    /// Retrieves the last coin detected
    std::shared_ptr<CircleImage> getLastCoin() const;
    
    /// Retrieves the features of the last coin
    CircleFeatures getLastCoinFeatures() const;
    
    
private:
    Mat bgr;
    Mat yCrCb;
    Mat highlight;
    Mat gray;
    Mat blur;
    std::shared_ptr<CircleImage> coinImage;
    std::vector<Mat> channels;
    Mat bgrCircle;
    int frameRate;
    VideoCapture capture;
};

#endif /* IMAGETASK_H */

