/** 
 * @file LVQTrainer.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date 15 de Outubro de 2016, 00:57
 */

#ifndef LVQTRAINER_H
#define LVQTRAINER_H

#include <cstdint>
#include <string>
#include "PatternTable.h"
#include "LVQNetwork.h"

/**
 * @brief Trainer for a Learning Vector Quantization Network
 * @param network The neural network to be trained and tested
 * @param trainingSet The truth table for the network's train set
 * @param initialAlpha Initial learning rate \f$\alpha_{1}\f$
 * @param finalAlpha Final learning rate \f$\alpha_{2}\f$
 * @param maxEpochs Sets the maximum number of epochs
 */
class LVQTrainer {
public:
    /**
     * Constructor
     * @param network The neural network to be trained and tested
     * @param trainingSet The truth table for the network's train set
     * @param initialAlpha Initial learning rate \f$\alpha_{1}\f$
     * @param finalAlpha Final learning rate \f$\alpha_{2}\f$
     * @param maxEpochs Sets the maximum number of epochs
     */
    LVQTrainer(LVQNetwork& _network, const PatternTable& _trainingSet, 
    float initialAlpha = 0.1f, float finalAlpha = 0.001f, 
    uint16_t maxEpochs = 10);

    virtual ~LVQTrainer() {
    };
    
    /**
     * Executes a single iteration of the training algorithm
     * @return Whether some training was performed (training is skipped when
     * the number of epochs exceed <i>maxEpochs</i>)
     */
    bool trainSingleEpoch();
    
    /// Check if more entries need to be tested
    bool hasNext() const;

private:
    /// Neural network reference
    LVQNetwork& net;
    
    /// Pattern table used for training
    const PatternTable& trainingSet;
    
    /// Initial learning rate \f$\alpha_{1}\f$
    float initialAlpha = 0.1;

    /// Final learning rate \f$\alpha_{2}\f$
    float finalAlpha = 0.01;

    /// Learning rate step \f$\Delta\alpha\f$
    float alphaStep;
    
    /// Current learning rate
    float currAlpha;

    /// Number of epochs for the training algorithm
    uint16_t maxEpochs;
    
    /// Current epoch
    uint16_t currEpoch;
    
};

#endif /* LVQTRAINER_H */