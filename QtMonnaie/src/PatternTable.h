/** 
 * @file PatternTable.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * 
 * @date November 6, 2016, 9:24 AM
 */

#ifndef PATTERNTABLE_H
#define PATTERNTABLE_H

#include <cstdint>
#include <vector>
#include <utility>
#include <iostream>

typedef std::pair< std::vector<float>, const uint8_t> TableEntry;

/// @brief A truth table for multiple input (16-bit each) and single output
class PatternTable {
public:
    /// Default constructor
    PatternTable();
    virtual ~PatternTable();

    /**
     * Adds a new entry to the table
     * @param input The input array
     * @param output The desired output for this pattern
     */
    void addEntry(std::vector<float> input, uint8_t output);

    /**
     * Gets the input for a given entry
     * @param index Valid entry index
     * @return The input pattern
     */
    std::vector<float> getInput(uint16_t index) const;

    /**
     * Gets the output for a given entry
     * @param index Valid entry index
     * @return The output
     */
    uint8_t getOutput(uint16_t index) const;

    /// Retrieves the number of entries in the table
    uint16_t size() const;

    /**
     * Adds a new entry to the table
     * @param entry A pair containing input pattern and output
     * @return 
     */
    PatternTable& operator+=(TableEntry entry);

    /**
     * Retrieves the nth entry from the table
     * @param idx Valid index
     * @return An std::pair containing input and output
     */
    TableEntry operator[](int idx) const;
    
    /**
     * Writes this pattern table as values separated by spaces
     * @param out Output stream
     */
    void write(std::ostream& out) const;
    
private:
    std::vector<std::vector<float>> inputs;
    std::vector<uint8_t> outputs;
};

#endif /* PATTERNTABLE_H */

