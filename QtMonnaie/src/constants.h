/**
 * @file constants.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date November 7, 2016, 9:46 PM
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#ifdef __cplusplus
extern "C" {
#endif

namespace Paths {
    constexpr auto InitSet = "data/init";
    constexpr auto TrainSet = "data/train";
    constexpr auto TestSet = "data/test";
    constexpr auto UnitTestSet1 = "data/unittest1";
    constexpr auto UnitTestSet2 = "data/unittest2";
    constexpr auto WeightsFile = "data/weights.txt";
}    


#ifdef __cplusplus
}
#endif

#endif /* CONSTANTS_H */

