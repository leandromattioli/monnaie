/** 
 * @file Chronometer.cpp
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * 
 * @date 5 de Dezembro de 2016, 00:07
 */

#include <chrono>

#include "Chronometer.h"

using std::chrono::steady_clock;
using std::chrono::duration_cast;
using std::chrono::microseconds;

Chronometer::Chronometer() {
    totalTime = 0;
}

void Chronometer::start() {
    if(running)
        return;
    begin = steady_clock::now();
    running = true;
}

unsigned long Chronometer::pause() {
    if(!running)
        return 0;
    end = steady_clock::now();
    totalTime += duration_cast<microseconds>(end - begin).count();
    running = false;
    return totalTime;
}

void Chronometer::reset() {
    totalTime = 0;
    running = false;
}

bool Chronometer::isRunning() const {
    return running;
}

unsigned long Chronometer::getEllapsed() const {
    return totalTime;
}




