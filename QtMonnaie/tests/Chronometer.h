/** 
 * @file Chronometer.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * 
 * @date 5 de Dezembro de 2016, 00:07
 */
#ifndef CHRONOMETER_H
#define CHRONOMETER_H

#include <chrono>

///Microseconds-precision chronometer
class Chronometer {
public:
    ///Constructor
    Chronometer();
    
    ///Starts or resumes time counting
    void start();
    
    ///Pauses time counting
    unsigned long pause();
    
    ///Reset chronometer to 0
    void reset();
    
    ///Get total ellapsed time
    unsigned long getEllapsed() const;
    
    ///Whether the chronometer is running
    bool isRunning() const;
private:
    std::chrono::steady_clock::time_point begin;
    std::chrono::steady_clock::time_point end;
    unsigned long totalTime;
    bool running;
};

#endif /* CHRONOMETER_H */

