/**
 * @file TestAvgColor.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * @date Nov 27, 2016, 2:54:21 PM
 */

#ifndef TESTAVGCOLOR_H
#define TESTAVGCOLOR_H

#include <cppunit/extensions/HelperMacros.h>
#include "constants.h"

class TestAvgColor : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestAvgColor);

    CPPUNIT_TEST(testAvgColor1);
    CPPUNIT_TEST(testAvgColor2);

    CPPUNIT_TEST_SUITE_END();
    

public:
    TestAvgColor();
    virtual ~TestAvgColor();
    void setUp();
    void tearDown();

private:
    std::vector<std::string> entries;
    std::string path1, path2;
    void testAvgColor1();
    void testAvgColor2();
};

#endif /* TESTAVGCOLOR_H */

