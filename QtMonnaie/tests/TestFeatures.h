/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TestFeatures.h
 * Author: hulk
 *
 * Created on November 28, 2016, 3:02 PM
 */

#ifndef TESTFEATURES_H
#define TESTFEATURES_H

#include <cppunit/extensions/HelperMacros.h>

class TestFeatures : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestFeatures);
    CPPUNIT_TEST(testFeatures);
    CPPUNIT_TEST_SUITE_END();

public:
    TestFeatures();
    virtual ~TestFeatures();
    void setUp();
    void tearDown();

private:
    std::vector<std::string> entries;
    std::string path;
    void testFeatures();
    void testNormalizedFeatures();
};

#endif /* TESTFEATURES_H */

