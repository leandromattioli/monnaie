/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TestUtils.h
 * Author: hulk
 *
 * Created on November 28, 2016, 2:10 AM
 */

#ifndef TESTUTILS_H
#define TESTUTILS_H

#include <vector>
#include <string>

class TestUtils {
public:
    static std::vector<std::string> getOrderedFiles(std::string path, bool fullpath=false);
};

#endif /* TESTUTILS_H */

