package com.leandromattioli.monnaie;

import java.text.SimpleDateFormat;

import org.opencv.core.Size;

import android.os.Environment;

public class Constants {
	public static final float[] clusterMap = {0.01f, 0.05f, 0.10f, 0.25f, 0.50f, 1.00f};
	public static final Size gaussianSize = new Size(7,7);
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyyMMdd_hhmmss");
	public static final short SIZE = 471;
	public static final short CLASSES = 6;
	public static final String IMG_TRAINING_DIR = Environment.getExternalStorageDirectory() + "/train/";
	public static final String IMG_TEST_DIR = Environment.getExternalStorageDirectory() + "/test/";
	public static final String IMG_INIT_DIR = Environment.getExternalStorageDirectory() + "/init/";
	public static final String OUTPUT_DIR = Environment.getExternalStorageDirectory() + "/out/";
	public static final String TAG = "MONNAIE";
}
