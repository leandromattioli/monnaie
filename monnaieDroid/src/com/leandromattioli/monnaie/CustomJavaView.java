package com.leandromattioli.monnaie;

import java.io.FileOutputStream;
import java.util.List;

import org.opencv.android.JavaCameraView;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.util.Log;


public class CustomJavaView extends JavaCameraView implements PictureCallback {

    private static final String TAG = "Circles::View";
    private String mPictureFileName;

    public CustomJavaView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    public boolean isReady() {
    	return mCamera != null;
    }

    public List<Size> getResolutionList() {
    	if(mCamera != null)
    		return mCamera.getParameters().getSupportedPreviewSizes();
        Log.e(TAG, "PROB. AO OBTER RESOLUÇÕES!");
    	return null;
    }

    public void setResolution(Size resolution) {
        disconnectCamera();
        mMaxHeight = (int) resolution.height;
        mMaxWidth = (int) resolution.width;
        connectCamera(getWidth(), getHeight());
    }
    
    public void setBrightnessContrast(float fraction) {
        Parameters params = mCamera.getParameters();
        //params.setExposureCompensation( (int) (params.getMaxExposureCompensation() * fraction) );
        params.setFlashMode(Parameters.FLASH_MODE_ON);
        mCamera.setParameters(params);
    }

    public Size getResolution() {
        return mCamera.getParameters().getPreviewSize();
    }
    
    public void takePicture(final String fileName) {
        this.mPictureFileName = fileName;
        // Postview and jpeg are sent in the same buffers if the queue is not empty when performing a capture.
        // Clear up buffers to avoid mCamera.takePicture to be stuck because of a memory issue
        mCamera.setPreviewCallback(null);
        // PictureCallback is implemented by the current class
        mCamera.takePicture(null, null, this);
    }
    
    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Saving a bitmap to file");
        // The camera preview was automatically stopped. Start it again.
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);

        // Write the image in a file (in jpeg format)
        try {
            FileOutputStream fos = new FileOutputStream(mPictureFileName);
            fos.write(data);
            fos.close();

        } catch (java.io.IOException e) {
            Log.e("MONNAIE", "Exception in photoCallback", e);
        }

    }

}