package com.leandromattioli.monnaie;
import org.opencv.android.NativeCameraView;

import android.content.Context;
import android.util.AttributeSet;

import org.opencv.highgui.Highgui;

public class CustomNativeView extends NativeCameraView {

	public CustomNativeView(Context context, int cameraId) {
		super(context, cameraId);
		config();
	}
	
	public CustomNativeView(Context context, AttributeSet attrs) {
		super(context, attrs);
		config();
	}
	
	public void config() {
		if(mCamera != null) {
			mCamera.set(Highgui.CV_CAP_PROP_FRAME_WIDTH, 320);
			mCamera.set(Highgui.CV_CAP_PROP_FRAME_HEIGHT, 240);
		}
	}

}
