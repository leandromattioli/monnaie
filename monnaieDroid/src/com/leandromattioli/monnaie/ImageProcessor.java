package com.leandromattioli.monnaie;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import android.util.Log;

public class ImageProcessor {
	
    //Frames
    private Mat currFrame;
    private Mat yCrCbFrame = new Mat();
    private Mat normalizedFrame = new Mat();
    private Mat grayFrame = new Mat();
    private Mat circleBGR = new Mat();
    private Mat circleYCrCb = new Mat();
    
    //Temp structures
    private Mat circleList = new Mat();
    
    //Coordinates
    private Point center = new Point();
    private int radius;
    
    
    public void setCurrFrame(Mat currFrame) {
    	this.currFrame = currFrame;
    }
	
    public Mat getModifiedFrame() {
    	return currFrame;
    }
	
    public Mat getCircle() {
    	return circleBGR;
    }
    
    public void showInfo(Short cluster) {
    	String message = String.format("R$ %.2f", Constants.clusterMap[cluster]);
    	Core.putText(currFrame, message, new Point(center.x, center.y), 
    			     Core.FONT_HERSHEY_SIMPLEX, 0.5, new Scalar(0,0,255));
    }
    
    public void preProcess() {
    	//Equalize histogram
    	ArrayList<Mat> channels = new ArrayList<Mat>();
    	Imgproc.cvtColor(currFrame, yCrCbFrame, Imgproc.COLOR_RGB2YCrCb);
    	Core.split(yCrCbFrame, channels);
    	Imgproc.equalizeHist(channels.get(0), channels.get(0));
        Core.merge(channels,yCrCbFrame);
        Imgproc.cvtColor(yCrCbFrame, normalizedFrame,Imgproc.COLOR_YCrCb2RGB);
        
        //Generating Gray Frame
    	Imgproc.cvtColor(normalizedFrame, grayFrame, Imgproc.COLOR_RGB2GRAY);
        //Imgproc.cvtColor(currFrame, grayFrame, Imgproc.COLOR_RGB2GRAY);
        Imgproc.GaussianBlur(grayFrame, grayFrame, Constants.gaussianSize, 2, 2);        
    }
    
    
    public boolean detectCircles() {
    	boolean found = false;
        Rect r;
        //Imgproc.equalizeHist(grayFrame, grayFrame); //normalized já é equalizada
        //Imgproc.threshold(grayFrame, grayFrame, 90, 255, Imgproc.THRESH_BINARY);
        //Imgproc.HoughCircles(grayFrame, circles, Imgproc.CV_HOUGH_GRADIENT, 
        //                     100, 30, 200, 100, 30, 35);
        Imgproc.HoughCircles(grayFrame, circleList, Imgproc.CV_HOUGH_GRADIENT, 
                             2, 300, 200, 100, 40, 0);
        if(circleList.total() > 0) {
            //Circle parameters
        	center.x = (int) Math.round(circleList.get(0, 0)[0]);
            center.y = (int) Math.round(circleList.get(0, 0)[1]);
            radius = (int) Math.round(circleList.get(0, 0)[2]);
            
            //Surrounding rectangle
            Point p1 = new Point();
            p1.x = center.x - radius;
            p1.y = center.y - radius;
            Point p2 = new Point();
            p2.x = p1.x + 2*radius;
            p2.y = p1.y + 2*radius;
            r = new Rect(p1, p2);
            
            if(p1.x > 0 && p2.x > 0 && p1.y > 0 && p2.y > 0) {
            	try {
            		ArrayList<Mat> channels = new ArrayList<Mat>();
            		circleYCrCb = new Mat(currFrame, r).clone();
            		Imgproc.cvtColor(circleYCrCb, circleYCrCb, Imgproc.COLOR_BGR2YCrCb);
            		Core.split(circleYCrCb, channels);
                	Imgproc.equalizeHist(channels.get(0), channels.get(0));
                    Core.merge(channels,circleYCrCb);
                    Imgproc.cvtColor(circleYCrCb, circleBGR,Imgproc.COLOR_YCrCb2BGR);
            		found = true;
            	}
            	catch(Exception e) { 
            		Log.e("MONNAIE", "Erro no frame!");
        		}
            }
        }
        return found;
    }
    
    public void highlightCircle() {
        Core.circle(currFrame, center, radius, new Scalar(0,0,200), 3, 8, 0);
    }
}