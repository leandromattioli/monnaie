package com.leandromattioli.monnaie;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

import com.leandromattioli.monnaie.lvq.LVQNetwork;
import com.leandromattioli.monnaie.lvq.LVQTrainer;
import com.leandromattioli.monnaie.lvq.PatternTable;

public class MonnaieActivity extends Activity implements CvCameraViewListener2, OnTouchListener {
    private static final String TAG = "Circles::Activity";
    
    //GUI
    private MenuItem[] menuItems;
    
    //Network
    private LVQNetwork net;
    private LVQTrainer trainer;
    private Short targetCluster = 0;
    private PatternTable trainingSet;
    private PatternTable testSet;
    
    //Camera
    private CameraBridgeViewBase camView;
    private CircleImage circle; 
    private ImageProcessor processor;
    private CustomJavaView customJavaView; //Custom Java View
//    private NativeCameraView mOpenCvNativeView; //Native Camera View
//    private JavaCameraView mOpenCvJavaView; //Java Camera View
//    private CustomNativeView customNativeView; //Custom Native View
    
    public void configCore() {
    	processor = new ImageProcessor();
        net = new LVQNetwork((short) 3, (short) 6);
        net.initWeights();
        /*try {
        	net.fromBinFile(getResources());
        }
        catch(Exception e) {
        	Log.e(Constants.TAG, "Arquivo de pesos inválido!");
        }*/
        trainer = new LVQTrainer();
        trainer.setNetwork(net);
        trainer.setInitialAlpha(0.01f);
        trainer.setFinalAlpha(0.001f);
        trainer.setEpochs(10);
        trainingSet = trainer.generateFeaturesTrainingSet();
        testSet = trainer.generateFeaturesTestSet();
        trainer.train(trainingSet);
        trainer.test(testSet);
        try {
			net.toBinFile(Constants.OUTPUT_DIR + "weights.bin");
		} catch (IOException e) {
			e.printStackTrace();
		}
       
    }
    

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    if(camView != null) {
                    	camView.enableView();
                    	camView.setOnTouchListener(MonnaieActivity.this);
                    }
                    configCore();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.monnaie_surface_view);
        //=========================================================================
        //CAMERA SWITCH
        //=========================================================================
        //Native Camera View
//        mOpenCvNativeView = (NativeCameraView) findViewById(R.id.native_camera_view);
//        mOpenCvNativeView.setVisibility(SurfaceView.VISIBLE);
//        mOpenCvNativeView.setCvCameraViewListener(this);
//        camView = (CameraBridgeViewBase) mOpenCvNativeView;

        //Java Camera View
//        mOpenCvJavaView = (JavaCameraView) findViewById(R.id.java_camera_view);
//        mOpenCvJavaView.setVisibility(SurfaceView.VISIBLE);
//        mOpenCvJavaView.setCvCameraViewListener(this);
//        camView = (CameraBridgeViewBase) mOpenCvJavaView;
        
        //Custom Native View
//        customNativeView = (CustomNativeView) findViewById(R.id.custom_native_view);
//        customNativeView.setVisibility(SurfaceView.VISIBLE);
//        customNativeView.setCvCameraViewListener(this);
//        customNativeView.config();
//        camView = (CameraBridgeViewBase) customNativeView;
        
        //Custom Java View
        customJavaView = (CustomJavaView) findViewById(R.id.custom_java_view);
        customJavaView.setVisibility(SurfaceView.VISIBLE);
        customJavaView.setCvCameraViewListener(this);
        camView = (CameraBridgeViewBase) customJavaView;
        //=========================================================================
        
    }
    
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
    	processor.setCurrFrame(inputFrame.rgba());
    	processor.preProcess();
    	if(processor.detectCircles()) {
    		circle = new CircleImage(processor.getCircle());
    		Short cluster = net.classify(circle.getFeatures());
    		/*Toast.makeText(this, message, Toast.LENGTH_SHORT).show();*/
    		processor.showInfo(cluster);
    	}
        return processor.getModifiedFrame();
    }
    

    @Override
    public void onPause() {
        super.onPause();
        if(camView != null)
        	camView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if(camView != null)
        	camView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
		checkCamera();
		configCamera();
		showResolution();
    }

    public void onCameraViewStopped() {}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	menuItems = new MenuItem[6];
    	menuItems[0] = menu.add("R$0,01");
    	menuItems[1] = menu.add("R$0,05");
    	menuItems[2] = menu.add("R$0,10");
    	menuItems[3] = menu.add("R$0,25");
    	menuItems[4] = menu.add("R$0,50");
    	menuItems[5] = menu.add("R$1,00");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
    	int i;
    	for(i = 0; i < 6; i++) {
    		if(item == menuItems[i])
    			targetCluster = (short) i;
    	}
    	Toast.makeText(this, menuItems[targetCluster].getTitle(), Toast.LENGTH_SHORT).show();
    	return true;
    }
    
    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
    	Mat circle = processor.getCircle();
    	Mat circleBgr = new Mat();
    	Imgproc.cvtColor(circle, circleBgr, Imgproc.COLOR_RGB2BGR);
    	if(circle == null)
    		return false;
		//Short cluster = getSelectedCluster();
		String fileName = Environment.getExternalStorageDirectory().getPath() + "/train/cluster_";
		fileName += targetCluster.toString() + "_";
		fileName += Constants.dateFormat.format(new Date()) + ".bmp";
		Highgui.imwrite(fileName, circleBgr);
		Toast.makeText(this, fileName + " saved", Toast.LENGTH_SHORT).show();
        return false;
    }
    

    /**
     * Checks whether the camera is available
     * @throws Exception 
     */
    public void checkCamera() {
    	if(customJavaView.isReady())
    		Log.i(TAG, "CAMERA DISPONIVEL");
    	else
    		Log.i(TAG, "CAMERA NAO DISPONIVEL");
    }
    
    /**
     * Configures camera parameters
     */
    public void configCamera() {
        List<Size> mResolutionList = customJavaView.getResolutionList();
        ListIterator<Size> resolutionItr = mResolutionList.listIterator();
        int idx = 0;
        while(resolutionItr.hasNext()) {
            Size element = resolutionItr.next();
            if(element.width == 320 && element.height == 240) {
            	customJavaView.setResolution(mResolutionList.get(idx));
            	break;
            }
            idx++;
         }
        customJavaView.setBrightnessContrast(0.9f);
    }
    
    /**
     * Shows current resolution as a Toast Message 
     */
    public void showResolution() {
        Size resolution = customJavaView.getResolution();
        String caption = Integer.valueOf(resolution.width).toString() + "x" + Integer.valueOf(resolution.height).toString();
        Toast.makeText(this, caption, Toast.LENGTH_SHORT).show();
    }
}
