package com.leandromattioli.monnaie.lvq;

import java.util.ArrayList;

public class PatternTable {
	private ArrayList<ArrayList<Byte>> inputs;
	private ArrayList<Integer> cluster;
	
	public PatternTable() {
		inputs = new ArrayList<ArrayList<Byte>>();
		cluster = new ArrayList<Integer>();
	}
	
	public void addEntry(ArrayList<Byte> inputs, int cluster) {
		this.inputs.add(inputs);
		this.cluster.add(cluster);
	}
	
	public ArrayList<Byte> getInputs(int entry) {
		return this.inputs.get(entry);
	}
	
	public Integer getCluster(int entry) {
		return this.cluster.get(entry);
	}
	
	public int size() {
		return inputs.size();
	}
}
