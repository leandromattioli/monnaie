/** 
 * @file circle.h
 * @author leandro
 *
 * @date 11 de Outubro de 2016, 10:50
 */

#ifndef CIRCLE_H
#define CIRCLE_H

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct {
        float x;
        float y;
        float radius;
    } Circle;


#ifdef __cplusplus
}
#endif

#endif /* CIRCLE_H */

