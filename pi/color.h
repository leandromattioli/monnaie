#ifndef COLOR_H_
#define COLOR_H_

typedef struct {
    unsigned short int c1;
    unsigned short int c2;
    unsigned short int c3;
    unsigned short int c4; //used only in 4-components color models (RGBA, CMYK)
} Color;

/*void ColorRGB2HSV(Color* rgb, Color* hsv);
void ColorRGB2YUV(Color* rgb, Color* yuv);*/


#endif /* COLOR_H_ */
