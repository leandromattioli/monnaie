/** 
 * @file debug_windows.c
 * @author leandro
 *
 * @date 11 de Outubro de 2016, 10:08
 */

#include <opencv/highgui.h>
#include "image.h"
#include "workimages.h"
#include "dbgwindows.h"

/**
 * Creates debug named windows for some images
 */
void dbg_windows_init() {
    cvNamedWindow("bgrSmooth", CV_WINDOW_AUTOSIZE);
    cvNamedWindow("y", CV_WINDOW_AUTOSIZE);
    cvNamedWindow("bgr", CV_WINDOW_AUTOSIZE);
}

/**
 * Updates the windows images
 * @param images
 */
void dbg_windows_show(WorkImages* images) {
    cvShowImage("bgrSmooth", images->bgrSmooth);
    cvShowImage("y", images->y);
    cvShowImage("bgr", images->bgr);
}

/**
 * Closes all windows
 */
void dbg_windows_destroy() {
    cvDestroyAllWindows();
}