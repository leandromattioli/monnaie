/** 
 * @file debug_windows.h
 * @author leandro
 *
 * @date 11 de Outubro de 2016, 10:08
 */

#ifndef DEBUG_WINDOWS_H
#define DEBUG_WINDOWS_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include "workimages.h"

void dbg_windows_init();
void dbg_windows_show(WorkImages* images);
void dbg_windows_destroy();


#ifdef __cplusplus
}
#endif

#endif /* DEBUG_WINDOWS_H */

