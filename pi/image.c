/**
 * @file image.c
 * @brief Basic operations on images (persistence and color at coordinate)
 */

#include <stdio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdbool.h>
#include "image.h"


/**
 * Loads an image from a file
 * @param filename Path to the file
 * @return The new Image entity
 */
Image image_create_from_file(char* filename) {
    return cvLoadImage(filename, 1);
}


/**
 * Destroys an Image entity.
 * This function should not be called to destroy images created by cvRetrieveFrame.
 * @param img An Image entity
 */
void image_destroy(Image* img) {
    cvReleaseImage(img);
}


/**
 * Saves the image to a file
 * @param img An Image entity
 * @param file A file path
 */
void image_save(Image img, char* file) {
    cvSaveImage(file, img, NULL); //@NOTE Omit last parameter for older versions of OpenCV
}


/**
 * Gets the RGB color of a pixel
 * @param rgbImg The RGB image to work with
 * @param rgb The color structure to save the results
 * @param x Pixel's x coordinate
 * @param y Pixel's y coordinate
 * @return 1 for success or 0 for failure
 */
short int image_get_rgb_color_at(Image rgbImg, Color *rgb, int x, int y) {
    int pixelPosition;
    uchar* ptr_rgb;
    if(!image_valid_coords(rgbImg, x, y))
        return 0;
    ptr_rgb = (uchar*) (rgbImg->imageData + y * rgbImg->widthStep);
    pixelPosition = 3*x;
    rgb->c1 = ptr_rgb[pixelPosition];
    rgb->c2 = ptr_rgb[pixelPosition + 1];
    rgb->c3 = ptr_rgb[pixelPosition + 2];
    return 1;
}


/**
 * Gets the HSV color of a pixel
 * @param rgbImg The RGB image to work with
 * @param hsv The color structure to save the results
 * @param x Pixel's x coordinate
 * @param y Pixel's y coordinate
 * @return 1 for success or 0 for failure (invalid coordinates)
 * TODO Check whether convert operation is hardware based (otherwise it will be slow)
 */
short int image_get_hsv_color_at(Image rgbImg, Color *hsv, int x, int y) {
    int pixelPosition;
    IplImage *hsvImg;
    if(!image_valid_coords(rgbImg, x, y))
        return 0;
    hsvImg = cvCreateImage(cvGetSize(rgbImg), IPL_DEPTH_8U, 3);
    cvCvtColor(rgbImg, hsvImg, CV_RGB2HSV);
    uchar* ptr_hsv = (uchar*) (hsvImg->imageData + y * hsvImg->widthStep);
    pixelPosition = 3*x;
    hsv->c1 = ptr_hsv[pixelPosition];
    hsv->c2 = ptr_hsv[pixelPosition + 1];
    hsv->c3 = ptr_hsv[pixelPosition + 2];
    cvReleaseImage(&hsvImg);
    return 1;
}


/**
 * Checks whether a pair of pixel coordinates is valid for a given image
 * @param img The image to work with
 * @param x Pixel's x coordinate
 * @param y Pixel's y coordinate
 * @return 1 for valid coordinates or 0 for invalid (outside the image)
 */
bool image_valid_coords(Image img, int x, int y) {
    if(x < 0 || x >= img->width || y < 0 || y >= img->height)
        return false;
    return true;
}
