#ifndef IMAGE_H_
#define IMAGE_H_

#include <opencv/highgui.h>
#include <opencv/cv.h>
#include <stdbool.h>
#include "color.h"

typedef IplImage* Image;

Image image_create_from_file(char* filename);
void image_destroy(Image* img);
void image_save(Image img, char* filename);
short int image_get_rgb_color_at(Image rgbImg, Color *rgb, int x, int y);
short int image_get_hsv_color_at(Image rgbImg, Color *hsv, int x, int y);
bool image_valid_coords(Image img, int x, int y);

#endif
