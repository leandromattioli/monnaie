/** 
 * @file lvq.h
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date 3 de Outubro de 2016, 09:51
 */

#ifndef LVQ_H
#define LVQ_H

#ifdef __cplusplus
extern "C" {
#end
    void lvq_init_weights(float weights[][]);


#ifdef __cplusplus
}
#endif

#endif /* LVQ_H */

