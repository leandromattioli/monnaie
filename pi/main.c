/**
 * @file main.c
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date 26 de Setembro de 2016, 11:10
 */

#include <stdio.h>
#include <stdlib.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <pthread.h>
#include <stdbool.h>
#include "webcam.h"
#include "image.h"
#include "moeda.h"
#include "workimages.h"
#include "dbgwindows.h"
#include "circle.h"

#define KEY_STEP 0.05
#define WEBCAM   1

bool shouldClose = false;

typedef struct {
    Webcam* cam;
    WorkImages* images;
} TaskOpenCVArgs;

void* task_opencv(void* args) {
    TaskOpenCVArgs* typedArgs = (TaskOpenCVArgs*) args;
    Webcam cam = *(typedArgs->cam);
    WorkImages images = *(typedArgs->images);
    Circle circle;
    char key;
    double brightness = webcam_get_brightness(cam);
    double contrast = webcam_get_contrast(cam);
    printf("Brilho=%lf  Contraste=%lf\n", brightness, contrast);
    while (1) {
        images.bgr = webcam_get_frame_as_bgr_image(cam);
        if (!(images.bgr)) {
            perror("Não foi possível obter o frame!");
            continue;
        }

        moeda_preprocessar(&images);
        moeda_detectar_moeda(&images, &circle);
        dbg_windows_show(&images);

        //Aguardando saída
        key = cvWaitKey(40);
        if (key == 27) {
            shouldClose = true;
            return NULL;
        } else if (key == 's') {
            image_save(images.bgr, "screenshot.jpg");
        }
        else if (key == 'a') {
            brightness += KEY_STEP;
            webcam_set_brightness(cam, brightness);
            printf("brilho=%lf\n", brightness);
        }
        else if (key == 'q') {
            brightness -= KEY_STEP;
            webcam_set_brightness(cam, brightness);
            printf("brilho=%lf\n", brightness);
        }
        else if (key == 'w') {
            contrast += KEY_STEP;
            webcam_set_contrast(cam, contrast);
            printf("contraste=%lf\n", contrast);
        }
        else if (key == 'e') {
            contrast -= KEY_STEP;
            webcam_set_contrast(cam, contrast);
            printf("contraste=%lf\n", contrast);
        }
    }
}

int main(int argc, char** argv) {
    WorkImages images;
    Webcam cam;
    TaskOpenCVArgs taskImgArgs;
    
    pthread_t threadOpenCV;

    //Inicializa Câmera
    webcam_init_with_device(&cam, WEBCAM);
    webcam_set_defaults(cam);
    if (!cam) {
        perror("Não foi possível inicializar a câmera\n!");
        exit(-1);
    }

    workimages_init(&images);
    taskImgArgs.cam = &cam;
    taskImgArgs.images = &images;
    pthread_create(&threadOpenCV, NULL, task_opencv, &taskImgArgs);

    dbg_windows_init();
    
    while(1) {
        //@TODO colocar GUI aqui!
        if(shouldClose)
            break;
    }
    
    webcam_destroy(&cam);
    workimages_destroy(&images);
    
    dbg_windows_destroy();

    return (EXIT_SUCCESS);
}