/** 
 * @file imgmoeda.c
 * @author Leandro Mattioli <leandro.mattioli@gmail.com>
 * @date 10 de Outubro de 2016, 13:50
 */

#include <stdbool.h>
#include <stdio.h> //debug
#include <opencv/cv.h>
#include "moeda.h"
#include "workimages.h"
#include "circle.h"

/**
 * Aplica filtros na imagem para facilitar a detecção do círculo
 * @param images Conjunto de imagens com a componente bgr já adquirida
 */
void moeda_preprocessar(WorkImages* images) {
    //Equalizando histograma
    cvCvtColor(images->bgr, images->yCrCb, CV_BGR2YCrCb);
    cvSplit(images->yCrCb, images->y, images->Cr, images->Cb, NULL);
    //cvEqualizeHist(images->y, images->y);
    cvMerge(images->y, images->Cr, images->Cb, NULL, images->yCrCb);
    cvCvtColor(images->yCrCb, images->bgrEqualized, CV_YCrCb2BGR);
    //bgrEqualized --> muito ruim!

    //Suavização
    cvSmooth(images->yCrCb, images->smooth, CV_MEDIAN, 7, 7, 0, 0);
    cvCvtColor(images->smooth, images->bgrSmooth, CV_YCrCb2BGR);
    //cvSplit(images->smooth, images->y, NULL, NULL, NULL);

    //Threshold
    cvThreshold(images->y, images->threshold, 70, 255, CV_THRESH_BINARY_INV);
}

/**
 * Detecta a moeda na imagem e obtém o centro e o raio
 * @param images Conjunto de imagens com a componente y já processada
 * @return 
 */
bool moeda_detectar_moeda(WorkImages* images, Circle* moeda) {
    CvSeq* seq;
    float* dados;
    int i;
    Circle current;
    bool found = false;
    CvMemStorage* storage = cvCreateMemStorage(0);
    seq = cvHoughCircles(images->y, storage, CV_HOUGH_GRADIENT,
            //2, 300, 200, 100, 40, 0);
            2, 100, 200, 100, 100, 200);
    //1, 20, 100, 100, 20, 200); //max rad = 240
    moeda->x = -1;
    moeda->y = -1;
    moeda->radius = -1;
    for (i = 0; i < seq->total; i++) {
        dados = (float*) cvGetSeqElem(seq, i);
        current.x = dados[i];
        current.y = dados[i + 1];
        current.radius = dados[i + 2];
        printf("%f %f %f\n", current.x, current.y, current.radius);
        if(image_valid_coords(images->bgr, current.x, current.y) && 
                              current.radius > 0 && current.radius < 240) {
            found = true;
            //Na estrutura circle, ficará apenas o de maior raio
            if(current.radius > moeda->radius) {
                moeda->radius = current.radius;
                moeda->x = current.x;
                moeda->y = current.y;
            }
        }
        cvCircle(images->bgr, cvPoint(current.x, current.y), 
                current.radius, cvScalar(255, 0, 0, 0), 2, 8, 0);
    }
    cvClearMemStorage(storage);
    return found;
}
