/* 
 * File:   imgmoeda.h
 * Author: Leandro Mattioli <leandro.mattioli@gmail.com>
 *
 * Created on 10 de Outubro de 2016, 13:50
 */

#ifndef IMGMOEDA_H
#define IMGMOEDA_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdbool.h>
#include "circle.h"
#include "image.h"
#include "workimages.h"

    void moeda_preprocessar(WorkImages* images);
    bool moeda_detectar_moeda(WorkImages* images, Circle* circle);


#ifdef __cplusplus
}
#endif

#endif /* IMGMOEDA_H */

