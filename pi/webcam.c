/**
 * @file webcam.c
 * @brief Implementation of Webcam object (Wrapper functions for OpenCV).
 */

#include <stdio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "webcam.h"


/**
 * Inits a webcam entity with the system's default camera
 * @param cam The Webcam structure that will be used for capturing
 */
void webcam_init(Webcam* cam) {
    *cam = cvCreateCameraCapture(-1);
    return;
}


/**
 * Webcam object constructor
 * @param cam The Webcam structure that will be used for capturing
 * @param deviceNo The device to be used (0 for the first device)
 */
void webcam_init_with_device(Webcam* cam, short int deviceNo) {
    *cam = cvCreateCameraCapture(deviceNo);
    return;
}


/**
 * Webcam object destructor
 * @param cam An existing and well initialized Webcam structure
 */
void webcam_destroy(Webcam* cam) {
    cvReleaseCapture(cam);
    *cam = NULL;
}


/**
 * Checks whether a Webcam object is intialized
 * @param cam A Webcam structure
 * @return 1 if the camera is initialized and 0 otherwise
 * TODO Integrate with D-Bus / Guile
 */
unsigned short int webcam_check_connection(Webcam cam) {
    return (cam) ? 1 : 0;
}


/**
 * Sets webcam frame width
 * @param cam An existing and well initialized Webcam structure
 * @param width Frame width
 * TODO Integrate with D-Bus / Guile
 */
void webcam_set_width(Webcam cam, unsigned short int width) {
    cvSetCaptureProperty(cam, CV_CAP_PROP_FRAME_WIDTH, width);
}


/**
 * Gets webcam frame width
 * @param cam An existing and well initialized Webcam structure
 * @return Frame width
 * TODO Integrate with D-Bus / Guile
 */
unsigned short int webcam_get_width(Webcam cam) {
    return cvGetCaptureProperty(cam, CV_CAP_PROP_FRAME_WIDTH);
}


/**
 * Sets webcam frame height
 * @param cam An existing and well initialized Webcam structure
 * @param height Frame height
 * TODO Integrate with D-Bus / Guile
 */
void webcam_set_height(Webcam cam, unsigned short int height) {
    cvSetCaptureProperty(cam, CV_CAP_PROP_FRAME_HEIGHT, height);
}


/**
 * Gets webcam frame height
 * @param cam An existing and well initialized Webcam structure
 * @return Frame height
 * TODO Integrate with D-Bus / Guile
 */
unsigned short int webcam_get_height(Webcam cam) {
    return cvGetCaptureProperty(cam, CV_CAP_PROP_FRAME_HEIGHT);
}


/**
 * Change Webcam's contrast
 * @param cam An existing and well initialized Webcam structure
 * @param contrast The new contrast value between 0.0 and 1.0
 * TODO Integrate with D-Bus / Guile
 */
void webcam_set_contrast(Webcam cam, double contrast) {
    cvSetCaptureProperty(cam, CV_CAP_PROP_CONTRAST, contrast);
}


/**
 * Get contrast from the Webcam capture device
 * @param cam An existing and well initialized Webcam structure
 * @return The current contrast, in the range between 0.0 and 1.0
 * TODO Integrate with D-Bus / Guile
 */
double webcam_get_contrast(Webcam cam) {
    return cvGetCaptureProperty(cam, CV_CAP_PROP_CONTRAST);
}


/**
 * Change Webcam's brightness
 * @param cam An existing and well initialized Webcam structure
 * @param brightness The new brightness value between 0.0 and 1.0
 * TODO Integrate with D-Bus / Guile
 */
void webcam_set_brightness(Webcam cam, double brightness) {
    cvSetCaptureProperty(cam, CV_CAP_PROP_BRIGHTNESS, brightness);
}


/**
 * Get brightness from capture device
 * @param cam An existing and well initialized Webcam structure
 * @return The current brightness, in the range between 0.0 and 1.0
 * TODO Integrate with D-Bus / Guile
 */
double webcam_get_brightness(Webcam cam) {
    return cvGetCaptureProperty(cam, CV_CAP_PROP_BRIGHTNESS);
}


/**
 * Sets defaults parameters (width, height, brightness and contrast) for a webcam
 * @param cam An existing and well initialized Webcam structure
 * TODO Integrate with D-Bus / Guile
 */
void webcam_set_defaults(Webcam cam) {
    webcam_set_width(cam, DEFAULT_WIDTH);
    webcam_set_height(cam, DEFAULT_HEIGHT);
    //WebcamSetBrightness(cam, DEFAULT_BRIGHTNESS);
    //WebcamSetContrast(cam, DEFAULT_CONTRAST);
}


/**
 * Gets a frame of the webcam as an Image entity
 * @param cam An existing and well initialized Webcam structure
 * @return A pointer to an new BGR image entity or NULL.
 */
Image webcam_get_frame_as_bgr_image(Webcam cam) {
    if(!cvGrabFrame(cam))
        return NULL;
    return cvRetrieveFrame(cam, 0);
}
