#ifndef WEBCAM_H_
#define WEBCAM_H_

#include <opencv/highgui.h>
#include "image.h"

#define DEFAULT_WIDTH 640
#define DEFAULT_HEIGHT 480
#define DEFAULT_PREVIEW_WINDOW_TITLE "Video"
#define DEFAULT_BRIGHTNESS 0.5
#define DEFAULT_CONTRAST 0.5

typedef CvCapture* Webcam;

void webcam_init(Webcam* cam);
void webcam_init_with_device(Webcam* cam, short int deviceNo);
void webcam_destroy(Webcam* cam);

unsigned short int webcam_check_connection(Webcam cam);

void webcam_set_width(Webcam cam, unsigned short int width);
unsigned short int webcam_get_width(Webcam cam);
void webcam_set_height(Webcam cam, unsigned short int height);
unsigned short int webcam_get_height(Webcam cam);
void webcam_set_contrast(Webcam cam, double contrast);
double webcam_get_contrast(Webcam cam);
void webcam_set_brightness(Webcam cam, double brightness);
double webcam_get_brightness(Webcam cam);
void webcam_set_defaults(Webcam cam);
Image webcam_get_frame_as_bgr_image(Webcam cam);


#endif
