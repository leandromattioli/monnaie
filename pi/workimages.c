/** 
 * @file workimages.c
 * @author leandro
 *
 * @date 11 de Outubro de 2016, 10:26
 */

#include <opencv/cv.h>
#include "workimages.h"
#include "webcam.h"

/**
 * Creates the set of images
 * @param images
 */
void workimages_init(WorkImages* images) {
    CvSize size = cvSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    images->yCrCb = cvCreateImage(size, IPL_DEPTH_8U, 3);
    images->y = cvCreateImage(size, IPL_DEPTH_8U, 1);
    images->Cr = cvCreateImage(size, IPL_DEPTH_8U, 1);
    images->Cb = cvCreateImage(size, IPL_DEPTH_8U, 1);
    images->smooth = cvCreateImage(size, IPL_DEPTH_8U, 3);
    images->threshold = cvCreateImage(size, IPL_DEPTH_8U, 1);
    images->bgrEqualized = cvCreateImage(size, IPL_DEPTH_8U, 3);
    images->bgrSmooth = cvCreateImage(size, IPL_DEPTH_8U, 3);
}

/**
 * Destroys the set of images
 * @param images
 */
void workimages_destroy(WorkImages* images) {
    image_destroy(&(images->bgr));
    image_destroy(&(images->yCrCb));
    image_destroy(&(images->smooth));
    image_destroy(&(images->bgrEqualized));
    image_destroy(&(images->threshold));
}