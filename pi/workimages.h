/** 
 * @file workimages.h
 * @author leandro
 *
 * @date 11 de Outubro de 2016, 10:10
 */

#ifndef WORKIMAGES_H
#define WORKIMAGES_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include "image.h"

    /**
     * Set of images for image processing
     */
    typedef struct {
        Image bgr;
        Image yCrCb;
        Image y;
        Image Cr;
        Image Cb;
        Image smooth;
        Image threshold;
        Image bgrEqualized;
        Image bgrSmooth;
        Image circleYCrCb;
    } WorkImages;
    
    void workimages_init(WorkImages* images);
    void workimages_destroy(WorkImages* images);


#ifdef __cplusplus
}
#endif

#endif /* WORKIMAGES_H */

