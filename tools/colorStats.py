#!/usr/bin/env python
#! -*- coding: utf-8 -*-

# LoadImage byte order is BGR for Windows bitmaps
# TODO: Re-check image iterator

import os
from math import sqrt
from collections import OrderedDict
import cv
import sys

#Constants
IMG_PATH = '../desktop/img/train'


class Statistics:
    def avg(self, vec3list):
        avgTmp = [0, 0, 0]
        for pixel in vec3list:
            avgTmp[0] += pixel[0]
            avgTmp[1] += pixel[1]
            avgTmp[2] += pixel[2]
        return [x / len(vec3list) for x in avgTmp]
    def variance(self, vec3list):
        avg = self.avg(vec3list)
        varianceTmp = [0.0, 0.0, 0.0]
        for pixel in vec3list:
            varianceTmp[0] += (pixel[0] - avg[0])**2
            varianceTmp[1] += (pixel[1] - avg[1])**2
            varianceTmp[2] += (pixel[2] - avg[2])**2
        return [x / len(vec3list) for x in varianceTmp]
    def stdDev(self, vec3list):
        return map(sqrt, self.variance(vec3list))
    def avgSingle(self, slist):
        tmp = 0
        for val in slist:
            tmp += val
        return tmp / len(slist)
    def varianceSingle(self, slist):
        avg = self.avgSingle(slist)
        varianceTmp = 0
        for val in slist:
            varianceTmp += (val - avg)**2
        return varianceTmp / len(slist)
    def stdDevSingle(self, slist):
        return sqrt(self.varianceSingle(slist))
s = Statistics()
        

class CircleImage:
    def __init__(self, mat):
        self._mat = mat
        self.width = mat.width
        self.height = mat.height
        self.rows = mat.rows
        self.cols = mat.cols
    def __len__(self):
        return len(CircleIterator(self._mat))
    def __iter__(self):
        return CircleIterator(self._mat)
        

class CircleIterator:
    def __init__(self, img, infLimit = 0, supLimit = 1):
        self._img = img
        radius = img.width / 2
        xc = img.width / 2
        yc = img.height / 2
        self._points = []
        for y in range(img.rows):
            for x in range(img.cols):
                r = (x - xc)**2 + (y - yc)**2
                if r <= (supLimit * radius)**2 and r >= (infLimit * radius)**2: 
                    self._points.append((x,y))
        self._currPoint = 0
    def __iter__(self):
        return self
    def __len__(self):
        return len(self._points)
    def next(self):
        if self._currPoint < len(self._points):
            x = self._points[self._currPoint][0]
            y = self._points[self._currPoint][1]
            pixelTmp = self._img[y, x]
            pixel = [pixelTmp[0] - 128, pixelTmp[1] - 128, pixelTmp[2] - 128]
            self._currPoint += 1
            #return pixelTmp #unsigned
            return pixel # -128 ... 127 (0x00 ... 0xFF)
        else:
            raise StopIteration

      
class ReportData:
    def __init__(self):
        self._data = OrderedDict()
        for cluster in range(6):
            cluster = str(cluster)
            self._data[cluster] = {}
            self._data[cluster]['bgrAvgValues'] = []
            self._data[cluster]['hsvAvgValues'] = []
            self._data[cluster]['yCrCbAvgValues'] = []
            self._data[cluster]['bgrSDValues'] = []
            self._data[cluster]['hsvSDValues'] = []
            self._data[cluster]['yCrCbSDValues'] = []
            self._data[cluster]['diameters'] = []
            self._data[cluster]['rDiff'] = []
    def append(self, cluster, bgr, hsv, yCrCb, bgrStdDev, hsvStdDev, yCrCbStdDev, diam, rdiff):
        self._data[cluster]['bgrAvgValues'].append(bgr)
        self._data[cluster]['hsvAvgValues'].append(hsv)
        self._data[cluster]['yCrCbAvgValues'].append(yCrCb)
        self._data[cluster]['bgrSDValues'].append(bgrStdDev)
        self._data[cluster]['hsvSDValues'].append(hsvStdDev)
        self._data[cluster]['yCrCbSDValues'].append(yCrCbStdDev)
        self._data[cluster]['diameters'].append(diam)
        self._data[cluster]['rDiff'].append(rdiff)
    def evaluateStats(self, cluster):
        #Average (images) of average color (pixels)
        self._data[cluster]['bgrAvgAvg'] = s.avg(self._data[cluster]['bgrAvgValues'])
        self._data[cluster]['hsvAvgAvg'] = s.avg(self._data[cluster]['hsvAvgValues']) 
        self._data[cluster]['yCrCbAvgAvg'] = s.avg(self._data[cluster]['yCrCbAvgValues'])
        
        #Standard deviation (images) of average color (pixels)
        self._data[cluster]['bgrSDAvg'] = s.stdDev(self._data[cluster]['bgrAvgValues'])
        self._data[cluster]['hsvSDAvg'] = s.stdDev(self._data[cluster]['hsvAvgValues']) 
        self._data[cluster]['yCrCbSDAvg'] = s.stdDev(self._data[cluster]['yCrCbAvgValues'])
        
        #Average (images) of standard deviation (pixels)
        self._data[cluster]['bgrAvgSD'] = s.avg(self._data[cluster]['bgrSDValues'])
        self._data[cluster]['hsvAvgSD'] = s.avg(self._data[cluster]['hsvSDValues']) 
        self._data[cluster]['yCrCbAvgSD'] = s.avg(self._data[cluster]['yCrCbSDValues'])
        
        #Standard deviation (images) of standard deviation (pixels)
        self._data[cluster]['bgrSDSD'] = s.stdDev(self._data[cluster]['bgrSDValues'])
        self._data[cluster]['hsvSDSD'] = s.stdDev(self._data[cluster]['hsvSDValues']) 
        self._data[cluster]['yCrCbSDSD'] = s.stdDev(self._data[cluster]['yCrCbSDValues'])
        
        #Average of radius
        self._data[cluster]['diamAvg'] = s.avgSingle(self._data[cluster]['diameters'])
        self._data[cluster]['diamStdDev'] = s.stdDevSingle(self._data[cluster]['diameters'])
        
        #Difference between colors far from center and colors near to center
        self._data[cluster]['rDiffAvg'] = s.avg(self._data[cluster]['rDiff'])
        self._data[cluster]['rDiffStdDev'] = s.stdDev(self._data[cluster]['rDiff'])
        
    def printData(self):
        for cluster in range(6):
            cluster = str(cluster)
            self.evaluateStats(cluster)
            
            #Colors
            bgrAvgAvg = ','.join(map(str, self._data[cluster]['bgrAvgAvg']))
            hsvAvgAvg =  ','.join(map(str, self._data[cluster]['hsvAvgAvg']))
            yCrCbAvgAvg = ','.join(map(str, self._data[cluster]['yCrCbAvgAvg']))
            bgrSDAvg = ','.join(map(str, self._data[cluster]['bgrSDAvg']))
            hsvSDAvg = ','.join(map(str, self._data[cluster]['hsvSDAvg']))
            yCrCbSDAvg = ','.join(map(str, self._data[cluster]['yCrCbSDAvg']))
            bgrAvgSD = ','.join(map(str, self._data[cluster]['bgrAvgSD']))
            hsvAvgSD = ','.join(map(str, self._data[cluster]['hsvAvgSD']))
            yCrCbAvgSD = ','.join(map(str, self._data[cluster]['yCrCbAvgSD']))
            bgrSDSD = ','.join(map(str, self._data[cluster]['bgrSDSD']))
            hsvSDSD = ','.join(map(str, self._data[cluster]['hsvSDSD']))
            yCrCbSDSD = ','.join(map(str, self._data[cluster]['yCrCbSDSD']))
            
            #Diameters
            diamAvg = self._data[cluster]['diamAvg']
            diamStdDev = self._data[cluster]['diamStdDev']
            #Color difference between big and short radius
            rDiffAvg = ','.join(map(str, self._data[cluster]['rDiffAvg']))
            rDiffStdDev = ','.join(map(str, self._data[cluster]['rDiffStdDev']))
            
            print '%s,%s, ,%s, ,%s, ,%s, ,%s, ,%s, ,%s, ,%s, ,%s, ,%s, ,%s, ,%s, ,%s,%s, ,%s,%s' % \
                 (cluster, bgrAvgAvg, hsvAvgAvg, yCrCbAvgAvg,
                  bgrSDAvg, hsvSDAvg, yCrCbSDAvg, bgrAvgSD, hsvAvgSD, yCrCbAvgSD,
                  bgrSDSD, hsvSDSD, yCrCbSDSD, diamAvg, diamStdDev, 
                  rDiffAvg, rDiffStdDev)


          
# =======================================================================================
# Main Procedure
# =======================================================================================
def main():
    report = ReportData()
    for i in os.listdir(IMG_PATH):
        path = os.path.join(IMG_PATH, i)
        cluster = i.split('_')[1]
        
        bgrMat = cv.LoadImageM(path)
        hsvMat = cv.CreateMat(bgrMat.rows, bgrMat.cols, cv.CV_8UC3)
        yCrCbMat = cv.CreateMat(bgrMat.rows, bgrMat.cols, cv.CV_8UC3)
        cv.CvtColor(bgrMat, hsvMat, cv.CV_BGR2HSV)
        cv.CvtColor(bgrMat, yCrCbMat, cv.CV_BGR2YCrCb)
        
        bgrCircle = CircleImage(bgrMat)
        hsvCircle = CircleImage(hsvMat)
        yCrCbCircle = CircleImage(yCrCbMat)
        
        bgrAvg = s.avg(bgrCircle)
        hsvAvg = s.avg(hsvCircle)
        yCrCbAvg = s.avg(yCrCbCircle)
        bgrStdDev = s.stdDev(bgrCircle)
        hsvStdDev = s.stdDev(hsvCircle)
        yCrCbStdDev = s.stdDev(yCrCbCircle)
        
        #Color difference between r > 0.75 * RADIUS and r < 0.75 * RADIUS 
        extCircleIter = CircleIterator(yCrCbMat, 0.75, 1)
        intCircleIter = CircleIterator(yCrCbMat, 0, 0.75)
        extRadiusColor = s.avg(extCircleIter)
        intRadiusColor = s.avg(intCircleIter)
        diffRadius = []
        diffRadius.append(extRadiusColor[0] - intRadiusColor[0])
        diffRadius.append(extRadiusColor[1] - intRadiusColor[1])
        diffRadius.append(extRadiusColor[2] - intRadiusColor[2])
        
        report.append(cluster, bgrAvg, hsvAvg, yCrCbAvg, 
                               bgrStdDev, hsvStdDev, yCrCbStdDev, 
                               bgrMat.cols, diffRadius)
    
    report.printData()
    sys.exit()

# =======================================================================================
# Test procedures
# =======================================================================================
def test():
    bgrMat = cv.LoadImageM('../desktop/img/test/test.bmp')
    print "[0, 30, 140]" , bgrMat[0,0]
    bgrCircle = CircleImage(bgrMat)
    yCrCbMat = cv.CreateMat(bgrMat.rows, bgrMat.cols, cv.CV_8UC3);
    cv.CvtColor(bgrMat, yCrCbMat, cv.CV_BGR2YCrCb);
    yCrCbCircle = CircleImage(yCrCbMat);
    bgrAvg = s.avg(bgrCircle) # -128, -98, 12
    print "MÉDIAS BGR: ", bgrAvg
    yCrCbAvg = s.avg(yCrCbCircle)
    print "MÉDIAS YCrCB: ", yCrCbAvg
    print "SAINDO."
    sys.exit()
    
def test2():
    bgrMat = cv.LoadImageM('../desktop/img/test/test2.bmp')
    yCrCbMat = cv.CreateMat(bgrMat.rows, bgrMat.cols, cv.CV_8UC3);
    cv.CvtColor(bgrMat, yCrCbMat, cv.CV_BGR2YCrCb);
    yCrCbCircle = CircleImage(yCrCbMat);
    #i = 0
    #for p in yCrCbCircle:
    #    print i, p[0], p[1], p[2]
    #    sys.stdout.flush()
    #    i += 1
    print "COMPRIMENTO: ", len(yCrCbCircle)
    yCrCbAvg = s.avg(yCrCbCircle);
    print "MÉDIA YCrCB: ", yCrCbAvg #[45, 166, 104] OU [-82, 38, -23] 
    print "SAINDO."
    sys.exit()
    
    
def test3():
    img = [x for x in os.listdir("../desktop/img/init")]
    for i in img:
        path = os.path.join("../desktop/img/init", i)
        bgrMat = cv.LoadImageM(path)
        yCrCbMat = cv.CreateMat(bgrMat.rows, bgrMat.cols, cv.CV_8UC3)
        cv.CvtColor(bgrMat, yCrCbMat, cv.CV_BGR2YCrCb)
        yCrCbCircle = CircleImage(yCrCbMat)
        yCrCbAvg = s.avg(yCrCbCircle)
        print os.path.basename(i) + " " + str(yCrCbAvg)
    
    print "SAINDO."
    sys.exit()
# =======================================================================================
    

main()
#test3()
