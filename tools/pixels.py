#!/usr/bin/env python

DIAMETER=40.0
qtdPixels = 0
for i in range( int(DIAMETER) ):
	for j in range( int(DIAMETER) ):
		vec = float(i)**2 + float(j)**2
		if(vec < (DIAMETER / 2) ** 2):
			print "%s,%s" % (i,j)
			qtdPixels += 1
print "%s pixels" % qtdPixels
