#!/usr/bin/env python

INITIAL_WEIGHTS = '../desktop/out/initialWeights_features.csv'

import csv
import subprocess
import sys

rows = []
with open(INITIAL_WEIGHTS) as f:
    reader = csv.reader(f, delimiter=';')
    for row in reader:
        rows.append(row[:-1])
    
crs = rows[0]
cbs = rows[1]
rs = rows[2]

points = zip(crs, cbs, rs)
print points

gnuplot = subprocess.Popen(['gnuplot', '-persist'], stdin=subprocess.PIPE)
gnuplot.stdin.write("set nokey \n")
for p in points:
    gnuplot.stdin.write("set arrow to %s \n" % ','.join(p))

gnuplot.stdin.write("splot '-' with points ls 1 \n")
for p in points:
    gnuplot.stdin.write(' '.join(p) + '\n')
gnuplot.stdin.write('e \n')
raw_input("Pressione qualquer tecla para sair...")
gnuplot.kill()