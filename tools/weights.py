#!/usr/bin/env python

# PROBLEM: Code exceeds 65536 byte limit

import sys
import csv

if not sys.argv[1]:
	print("Must supply file path...")

filePath = sys.argv[1]
with open(filePath, 'rb') as csvfile:
	reader = csv.reader(csvfile, delimiter=';')
	i = 0
	print('// ### Macro start')
	for row in reader:
		for j in range( len(row) - 1 ):
			print('weights[%s][%s] = %sf;' % (i, j, row[j]))
		i += 1
	print('// ### Macro end')
